<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Syarat extends Model
{
    protected $table = "tabel_syarat";
    protected $fillable = [
        'id_paket', 'syarat'
    ];
    public $timestamps = false;
}
