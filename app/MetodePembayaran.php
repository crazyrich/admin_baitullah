<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MetodePembayaran extends Model
{
    protected $table = "tabel_metode_pembayaran";

    protected $fillable = [
        'nama','tutorial', 'author', 'updater', 'status'
    ];
}
