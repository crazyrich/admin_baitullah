<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model
{
    protected $table = "tabel_pembayaran";

    protected $fillable = [
        'id_pemesanan', 'kode_pembayaran', 'status_pembayaran', 'bukti_pembayaran', 'konfirmasi', 'konfirmasi_oleh',
        'author', 'created_at', 'updated_at'
    ];

}
