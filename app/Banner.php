<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    public $table = "tabel_banner";
    protected $fillable = ["path", "updater", "author", "status"];
}
