<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RuteMaskapai extends Model
{
    protected $table = "tabel_rute_maskapai";

    protected $fillable = [
        'id_paket', 'maskapai', 'dari', 'ke', 'pemberangkatan', 'author', 'updater'
    ];
}
