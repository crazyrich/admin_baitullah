<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dokumen extends Model
{
    public $table = "master_dokumen";
    protected $fillable = ["dokumen", "author"];
}
