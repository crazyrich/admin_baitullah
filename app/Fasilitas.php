<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fasilitas extends Model
{
    protected $table = "tabel_fasilitas";

    protected $fillable = [
        'id_paket', 'include', 'n_include', 'author', 'updater'
    ];
}
