<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pemesanan extends Model
{
    protected $table = "pemesanan";

    protected $fillable = [
        'user_id', 'user_data_id', 'tipe', 'paket_id', 'id_harga_paket', 'id_travel', 'author', 'updater'
    ];

    public function pembayaran()
    {
        return $this->hasOne('App\Pembayaran', 'id_pemesanan', 'id');
    }
}
