<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = "tabel_kategori_paket";

    protected $fillable = [
        'kategori_paket', 'author', 'updated', 'updater', 'status', 'created'
    ];

    public $timestamps = false;

    public function paket()
    {
        $this->hasMany(Paket::class);
    }
}
