<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ketentuan extends Model
{
    protected $table = "tabel_ketentuan";
    protected $fillable = [
        'id_paket', 'ketentuan'
    ];
    public $timestamps = false;
}
