<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Maskapai;
use Illuminate\Support\Facades\Validator;

class MaskapaiController extends Controller
{
    public function __construct()
    {
        $this->url = env('APP_URL')."/icon/maskapai/";
    }

    public function index()
    {
        $date = new \DateTime();
        $kode_mkp = "MKP".$date->getTimestamp();
        return view('maskapai.create', ["kode_mkp"=>$kode_mkp]);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'kode_maskapai' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'no_tlp' => 'required',
            'no_fax' => 'required',
            'website' => 'required',
            'icon' => 'required'
        ]);
        $maskapai_data = $request->all();
        $maskapai_data["creator"] = Auth::user()->name;
        $maskapai_data["updater"] = Auth::user()->name;
        $imageName = time().'.'.$request->file('icon-maskapai')->getClientOriginalExtension();
        $request->file('icon-maskapai')->move(public_path('icon/maskapai'), $imageName);
        $maskapai_data["icon"] = $imageName;
        try {
            $maskapai = Maskapai::create($maskapai_data);
            return redirect()->route('home-maskapai')->with('success','Maskapai berhasil dibuat');
        } catch (\Exception $e) {
            return redirect()->route('home-maskapai')->with('error', $e->getMessage());
        }
    }

    public function daftar(Request $request)
    {
        $start = $request->get('start');
        $length = $request->get('length');
        $maskapai_json = new \stdClass();
        $maskapai_json->draw = $request->get('draw');
        $order_props = $request->get('order');
        $order_col = $order_props[0]['column'];
        $order_dir = $order_props[0]['dir'];
        if ($request->get('columns')[$order_col]["data"] == "no") {
            $maskapai = Maskapai::skip($start)->take($length)->get();
        } else {
            $maskapai = Maskapai::orderBy($request->get('columns')[$order_col]["data"], $order_dir)->skip($start)->take($length)->get();
        }
        $maskapai_json->recordsTotal = Maskapai::all()->count();
        $maskapai_json->recordsFiltered = Maskapai::all()->count();
        $no = $start+1;
        foreach($maskapai as $key => $value)
        {
            $value->no = $no;
            $no+=1;
        }
        $maskapai_json->data = $maskapai;
        return json_encode($maskapai_json);
    }

    public function all()
    {
        return view('maskapai.daftar');
    }

    public function delete(Request $request)
    {
        $id = $request->get('id_admin');
        $maskapai = Maskapai::where('id','=',$id)->delete();
        return redirect()->back()->with('error','Maskapai berhasil dihapus');
    }

    public function edit(Request $request)
    {
        $maskapai = Maskapai::where('id', '=', $request->get('id_admin'))->first();
        return view('maskapai.edit', ['maskapai'=>$maskapai]);
    }

    public function update(Request $request)
    {
        $check_img = $request->file('icon-maskapai');
        if (!$check_img) {
            $param = $request->except('icon-maskapai');
            $travel = Maskapai::where('kode_maskapai', '=', $param["kode_maskapai"])->update(
                [
                    'nama'=> $param["nama"],
                    'no_tlp'=> $param["no_tlp"],
                    'no_fax'=> $param["no_fax"],
                    'website'=> $param["website"],
                    'alamat'=> $param["alamat"],
                ]
            );
            return redirect()->route('home-maskapai')->with('success','Maskapai '.$request->nama.' berhasil diperbarui');
        } else {
            $param = $request->except('icon-maskapai');
            $icon = $request->file('icon-maskapai');
            $imageName = time().'.'.$icon->getClientOriginalExtension();
            $icon->move(public_path('icon/maskapai'), $imageName);
            $travel = Maskapai::where('kode_maskapai', '=', $param["kode_maskapai"])->update(
                [
                    'nama'=> $param["nama"],
                    'no_tlp'=> $param["no_tlp"],
                    'no_fax'=> $param["no_fax"],
                    'website'=> $param["website"],
                    'alamat'=> $param["alamat"],
                    'icon'=> $this->url.$imageName,
                ]
            );
            return redirect()->route('daftar-maskapai')->with('success','Maskapai '.$request->nama.' berhasil diperbarui');
        }
    }
}
