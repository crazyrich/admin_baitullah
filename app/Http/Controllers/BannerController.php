<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Banner;

class BannerController extends Controller
{
    public function index()
    {
        return view('admin.banner.index');
    }

    public function save(Request $request)
    {
        $imageName = time().'.'.$request->file('banner')->getClientOriginalExtension();
        $request->file('banner')->move(public_path('banner'), $imageName);
        $banner = array();
        $banner["path"] = $imageName;
        $banner["status"]= 0;
        $banner["author"]=$request->user()->email;
        $banner["updater"]=$request->user()->email;
        try {
            Banner::create($banner);
            return redirect()->back()->with("success", "Banner berhasil disimpan");
        } catch (\Exception $e)
        {
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function daftar(Request $request)
    {
        $start = $request->get('start');
        $length = $request->get('length');
        $banner_json = new \stdClass();
        $banner_json->draw = $request->get('draw');
        $order_props = $request->get('order');
        $order_col = $order_props[0]['column'];
        $order_dir = $order_props[0]['dir'];
        if ($request->get('columns')[$order_col]["data"] == "no") {
            $banner = Banner::skip($start)->take($length)->get();
        } else {
            $banner = Banner::orderBy($request->get('columns')[$order_col]["data"], $order_dir)->skip($start)->take($length)->get();
        }
        $banner_json->recordsTotal = Banner::all()->count();
        $banner_json->recordsFiltered = Banner::all()->count();
        $no = $start+1;
        foreach($banner as $key => $value)
        {
            $value->no = $no;
            $no+=1;
        }
        $banner_json->data = $banner;
        return json_encode($banner_json);
    }

    public function delete(Request $request)
    {
        Banner::where("id", $request->get('id_admin'))->delete();
        return redirect()->back()->withErrors("Banner berhasil dihapus");
    }

    public function update(Request $request)
    {
        $status = $request->get('sts-banner');
        try {
            Banner::where("id",$request->get('id-banner'))->update(["status"=>$status]);
            return redirect()->back()->with(["success","Banner berhasil dirubah"]);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors("Opps, telah terjadi kesalahan pada sistem.");
        }
    }
}
