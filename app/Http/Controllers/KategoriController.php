<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Kategori;
use DB;

class KategoriController extends Controller
{
    public function index()
    {
        return view('kategori.create');
    }

    public function create(Request $request)
    {
        $data = $request->all();
        $dt = new \DateTime();
        $data["created"] = $dt->format('Y-m-d H:i:s');
        $data["updated"] = $dt->format('Y-m-d H:i:s');
        $data["author"] = Auth::user()->name."|".Auth::user()->kode_travel;
        $data["status"] = 1;
        try {
            $kateogri_result = Kategori::create($data);
            return back()->with('success','Kategori berhasil dibuat : '.$kateogri_result["kategori_paket"]);
        } catch (\Exception $e) {
            return back()->with('error',$e->getMessage());
        }

    }

    public function getAll()
    {
        return DataTables::of(Kategori::all())->make(true);
    }

    public function daftarKategori()
    {
        $kategori = DB::table('tabel_kategori_paket')->paginate(10);
        return view('kategori.daftar', ["kategori"=>$kategori]);
    }

    public function daftarKategoriJson()
    {
        $start = $request->get('start');
        $length = $request->get('length');
        $kategori_json = new \stdClass();
        $kategori_json->draw = $request->get('draw');
        $order_props = $request->get('order');
        $order_col = $order_props[0]['column'];
        $order_dir = $order_props[0]['dir'];
        if ($request->get('columns')[$order_col]["data"] == "no") {
            $kategori = Kategori::skip($start)->take($length)->get();
        } else {
            $kategori = Kategori::orderBy($request->get('columns')[$order_col]["data"], $order_dir)->skip($start)->take($length)->get();
        }
        $kategori_json->recordsTotal = SumberArtikel::all()->count();
        $kategori_json->recordsFiltered = SumberArtikel::all()->count();
        $no = $start+1;
        foreach($kategori as $key => $value)
        {
            $value->no = $no;
            $no+=1;
        }
        $kategori_json->data = $kategori;
        return json_encode($artikel_json);
    }

    public function delete(Request $request)
    {
        $id = $request->get('id');
        try {
            $kategori = Kategori::where('id_kategori_paket', '=', $id)->delete();
            return redirect()->back()->with("success", "Kategori berhasil dihapus");
        } catch (\Exception $e) {
            return redirect()->back()->with("error", $e->getMessage());
        }
        
    }
}
