<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pemesanan;
use Auth;
use App\Pembayaran;
use App\Kategori;
use App\Hotel;
use App\Paket;
use DB;

class PemesananController extends Controller
{
    public function getPemesanan()
    {
        $user = Auth::user()->travel;
        // $data = Pemesanan::where('id_travel','=', $user->id_travel)->where("")->first();
        $data = Pembayaran::where('status_pembayaran','=',"MENUNGGU KONFIRMASI")->where('pemesanan.id_travel','=', $user->id_travel)
                                ->join('pemesanan','tabel_pembayaran.id_pemesanan', '=', 'pemesanan.id')
                                ->select('pemesanan.*','tabel_pembayaran.*')->get();
        return $data->count();
    }

    public function getProsesPemesananJson(Request $request)
    {
        $user = Auth::user()->travel;
        $start = $request->get('start');
        $length = $request->get('length');
        $pemesanan_json = new \stdClass();
        $pemesanan_json->draw = $request->get('draw');
        $order_props = $request->get('order');
        $order_col = $order_props[0]['column'];
        $order_dir = $order_props[0]['dir'];
        if ($request->get('columns')[$order_col]["data"] == "no") {
            $data = Pembayaran::where('status_pembayaran','=',"MENUNGGU KONFIRMASI")->where('pemesanan.id_travel','=', $user->id_travel)
                        ->join('pemesanan','tabel_pembayaran.id_pemesanan', '=', 'pemesanan.id')
                        ->select('pemesanan.*','tabel_pembayaran.*')->skip($start)->take($length)->get();
        } else {
            $data = Pembayaran::where('status_pembayaran','=',"MENUNGGU KONFIRMASI")->where('pemesanan.id_travel','=', $user->id_travel)
                        ->join('pemesanan','tabel_pembayaran.id_pemesanan', '=', 'pemesanan.id')
                        ->select('pemesanan.*','tabel_pembayaran.*')->orderBy('pemesanan.'.$request->get('columns')[$order_col]["data"], $order_dir)->skip($start)->take($length)->get();
        }
        $pemesanan_json->recordsTotal = Pembayaran::where('status_pembayaran','=',"MENUNGGU KONFIRMASI")->where('pemesanan.id_travel','=', $user->id_travel)
                                                        ->join('pemesanan','tabel_pembayaran.id_pemesanan', '=', 'pemesanan.id')
                                                        ->select('pemesanan.*','tabel_pembayaran.*')->get()->count();
        $pemesanan_json->recordsFiltered = Pembayaran::where('status_pembayaran','=',"MENUNGGU KONFIRMASI")->where('pemesanan.id_travel','=', $user->id_travel)
                                                            ->join('pemesanan','tabel_pembayaran.id_pemesanan', '=', 'pemesanan.id')
                                                            ->select('pemesanan.*','tabel_pembayaran.*')->get()->count();
        $no = $start+1;
        foreach($data as $key => $value)
        {
            $paket = Paket::where('id_paket', '=', $value->paket_id)->first();
            $value->no = $no;
            $value->paket = $paket->nama_paket;
            $no+=1;
        }
        $pemesanan_json->data = $data;
        return json_encode($pemesanan_json);
    }

    public function belumDiProses()
    {
        return view('order.menunggu_konfirmasi');
    }

    public function openImage(Request $request)
    {
        $img = Pembayaran::select('bukti_pembayaran')->where('id_pemesanan',$request->get('id'))->first();
        return $img;
    }

    public function konfirmasiPemesanan(Request $request)
    {
        $id = $request->id_admin;
        $pembayaran = Pembayaran::where('id_pemesanan',$id)
                                        ->update(["status_pembayaran"=>"SUDAH DIKONFIRMASI",
                                                    "konfirmasi"=>1, "konfirmasi_oleh"=>$request->user()->email]);
        return redirect()->back()->with('success','Konfirmasi berhasil');
    }

    public function allPemesanan()
    {
        return view('order.daftar');
    }

    public function allPemesananJson(Request $request)
    {
        $user = Auth::user()->travel;
        $start = $request->get('start');
        $length = $request->get('length');
        $pemesanan_json = new \stdClass();
        $pemesanan_json->draw = $request->get('draw');
        $order_props = $request->get('order');
        $order_col = $order_props[0]['column'];
        $order_dir = $order_props[0]['dir'];
        if ($request->get('columns')[$order_col]["data"] == "no") {
            $data = Pembayaran::where('pemesanan.id_travel','=', $user->id_travel)
                        ->join('pemesanan','tabel_pembayaran.id_pemesanan', '=', 'pemesanan.id')
                        ->select('pemesanan.*','tabel_pembayaran.*')->skip($start)->take($length)->get();
        } else {
            $data = Pembayaran::where('pemesanan.id_travel','=', $user->id_travel)
                        ->join('pemesanan','tabel_pembayaran.id_pemesanan', '=', 'pemesanan.id')
                        ->select('pemesanan.*','tabel_pembayaran.*')->orderBy('pemesanan.'.$request->get('columns')[$order_col]["data"], $order_dir)->skip($start)->take($length)->get();
        }
        $pemesanan_json->recordsTotal = Pembayaran::where('pemesanan.id_travel','=', $user->id_travel)
                                                        ->join('pemesanan','tabel_pembayaran.id_pemesanan', '=', 'pemesanan.id')
                                                        ->select('pemesanan.*','tabel_pembayaran.*')->get()->count();
        $pemesanan_json->recordsFiltered = Pembayaran::where('pemesanan.id_travel','=', $user->id_travel)
                                                            ->join('pemesanan','tabel_pembayaran.id_pemesanan', '=', 'pemesanan.id')
                                                            ->select('pemesanan.*','tabel_pembayaran.*')->get()->count();
        $no = $start+1;
        foreach($data as $key => $value)
        {
            $paket = Paket::where('id_paket', '=', $value->paket_id)->first();
            $value->no = $no;
            $value->paket = $paket->nama_paket;
            $no+=1;
        }
        $pemesanan_json->data = $data;
        return json_encode($pemesanan_json);
    }

    public function tambah(Request $request)
    {
        $kategori = Kategori::select('id_kategori_paket','kategori_paket')->get();
        return view('order.buat_order', ['kategori'=>$kategori]);
    }

    public function simpan(Request $request)
    {
        $id_paket = $request->get('id_paket');
        $nama = $request->get('nama');
        $email = $request->get('email');
        $phone = $request->get('phone');
        $gender = $request->get('gender');
        $tgl_lahir = $request->get('tgl_lahir');
        $tipe_kamar = $request->get('tipe-kamar');
        $id_travel = $request->get('id_travel');
        $pemesanan = new Pemesanan();
        $pemesanan->user_id = auth()->user()->id;
        $pemesanan->tipe = "admin";
        $pemesanan->paket_id = $id_paket;
        $pemesanan->id_travel = $id_travel;
        $pemesanan->author = auth()->user()->email;
        $pemesanan->updater = auth()->user()->email;
        $pemesanan->created_at = date('Y-m-d H:i:s');
        $pemesanan->updated_at = date('Y-m-d H:i:s');
        $pemesanan->save();

        // INPUT DATA JEMAAH
        $jemaah = DB::table('user_data')->insert([
            "nama_lengkap" => $nama,
            "gender" => $gender,
            "no_hp" => $phone,
            "alamat_email" => $email,
            "tgl_lahir" => date('Y-m-d H:i:s', strtotime($tgl_lahir)),
            "author" => $request->user()->id,
            "updater" => $request->user()->id,
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ]);
        $id_user = DB::getPdo()->lastInsertId();
        // INPUT TABEL CHECKOUT
        $checkout = DB::table('tabel_checkout')->insert([
            "id_pemesanan" => $pemesanan->id,
            "id_pemesan" => 0,
            "id_paket" => $id_paket,
            "id_user_data" => $id_user,
            "room_type" => $tipe_kamar,
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ]);
        $id_checkout = DB::getPdo()->lastInsertId();
        $kode_pembayaran = DB::table('tabel_pembayaran')->select('kode_pembayaran')->orderBy('id', 'DESC')->first();
        if(!$kode_pembayaran) {
            $kode = "ODR0001";
        } else {
            $kode = $this->generateCode($kode_pembayaran->kode_pembayaran);
        }                                
        $pembayaran = DB::table('tabel_pembayaran')->insert(["id_pemesanan"=> $pemesanan->id, "kode_pembayaran"=>$kode, "author"=>"admin",  "created_at"=>date("Y/m/d H:i:s"), "updated_at"=>date("Y/m/d H:i:s")]);
        return redirect('pesanan/all');
    }

    public function generateCode($kode)
    {
        $kode_split = substr($kode, 3);
        $kode_init = substr($kode, 0, -4);
        return $kode_init.str_pad($kode_split+1, 4, "0", STR_PAD_LEFT);
    }
}
