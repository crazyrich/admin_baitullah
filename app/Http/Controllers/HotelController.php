<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hotel;
use DB;

class HotelController extends Controller
{
    public function __construct()
    {
        $this->url = env('APP_URL');
    }

    public function generateCode($kode)
    {
        $kode_split = substr($kode, 3);
        $kode_init = substr($kode, 0, -4);
        return $kode_init.str_pad($kode_split+1, 4, "0", STR_PAD_LEFT);
    }

    public function index()
    {
        return view('hotel.daftar');
    }

    public function buat()
    {
        return view('hotel.create');
    }

    public function create(Request $request)
    {
        $request->validate([
            "name"=>"required",
            "alamat"=>"required",
            "bintang"=>"required",
            "no_tlp"=>"required"
        ]);
        $data = $request->all();
        if($request->file('icon')){
            $icon = $request->file('icon');
            $imageName = time().'.'.$icon->getClientOriginalExtension();
            $icon->move(public_path('hotel/icon'), $imageName);
            $data["icon"] = time().strtolower(str_replace(" ","",$data["name"]));
        } else {
            $data["icon"] = "";
        }
        try {
            $hotel = Hotel::create($data);
            $no = 1;
            foreach($request->file('gambar-hotel') as $gambar)
            {
                $imageName = time().'_'.str_replace(" ","-",$hotel->name)."_".$no.'.'.$gambar->getClientOriginalExtension();
                $gambar->move(public_path('hotel'), $imageName);
                DB::table('trs_gambar_hotel')->insert(["id_hotel"=>$hotel->id, "image"=>"hotel/".$imageName,"display_order"=>$no]);
                $no+=1;
            }
            return redirect()->route('home-hotel')->with("success","Hotel berhasil dibuat.");
        } catch (\Exception $e) {
            return redirect()->back()->with("error","Opps, something wrong. Please check input your data".$e->getMessage());
        }

    }

    public function getHotel(Request $request)
    {
        $start = $request->get('start');
        $length = $request->get('length');
        $hotel_json = new \stdClass();
        $hotel_json->draw = $request->get('draw');
        $order_props = $request->get('order');
        $order_col = $order_props[0]['column'];
        $order_dir = $order_props[0]['dir'];
        if ($request->get('columns')[$order_col]["data"] == "no") {
            $hotel = Hotel::skip($start)->take($length)->get();
        } else {
            $hotel = Hotel::orderBy($request->get('columns')[$order_col]["data"], $order_dir)->skip($start)->take($length)->get();
        }
        $hotel_json->recordsTotal = Hotel::all()->count();
        $hotel_json->recordsFiltered = Hotel::all()->count();
        $no = $start+1;
        foreach($hotel as $key => $value)
        {
            $value->no = $no;
            $no+=1;
        }
        $hotel_json->data = $hotel;
        return json_encode($hotel_json);
    }

    public function delete(Request $request)
    {
        $id = $request->get('id_admin');
        $maskapai = Hotel::where('id','=',$id)->delete();
        return redirect()->back()->with('error','Hotel berhasil dihapus');
    }

    public function edit(Request $request)
    {
        $hotel = Hotel::where('id', '=', $request->get('id_admin'))->first();
        return view('hotel.edit', ['hotel'=>$hotel]);
    }

    public function update(Request $request)
    {
        $hotel = Hotel::where('id', '=', $request->get('id'))->update([
                "name"=>$request->get('name'),
                "alamat"=>$request->get('alamat'),
                "bintang"=>$request->get('bintang'),
                "no_tlp"=>$request->get('no_tlp'),
                "website"=>$request->get('website'),
                "updated_at"=>date('Y-m-d H:i:s')
        ]);
        return redirect()->route('home-hotel')->with('success','Hotel '.$request->name.' berhasil diperbarui');
    }

    public function daftarLokasi(Request $request)
    {
        $lokasi = $request->get('lokasi');
        $hotel = Hotel::where('lokasi', $lokasi)->get();
        return $hotel;
    }
}
