<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\MetodePembayaran;

class MetodePembayaranController extends Controller
{
    
    public function tambah(Request $request)
    {
        if($request->user()->kode_travel != 'SUPERADMIN') {
            abort('401');
        }
        return view('admin.pembayaran.tambah');
    }

    public function save(Request $request)
    {
        $data = $request->all();
        $data["author"] = $request->user()->email;
        $data["updater"] = $request->user()->email;
        $pembayaran = MetodePembayaran::create($data);
        return redirect()->back()->with('success', 'Berhasil menambahkan metode pembayaran baru');
    }

    public function daftar()
    {
        return view('admin.pembayaran.daftar');
    }

    public function daftarJson(Request $request)
    {
        $start = $request->get('start');
        $length = $request->get('length');
        $mp_json = new \stdClass();
        $mp_json->draw = $request->get('draw');
        $order_props = $request->get('order');
        $order_col = $order_props[0]['column'];
        $order_dir = $order_props[0]['dir'];
        if ($request->get('columns')[$order_col]["data"] == "no") {
            $mp = MetodePembayaran::skip($start)->take($length)->get();
        } else {
            $mp = MetodePembayaran::orderBy($request->get('columns')[$order_col]["data"], $order_dir)->skip($start)->take($length)->get();
        }
        $mp_json->recordsTotal = MetodePembayaran::all()->count();
        $mp_json->recordsFiltered = MetodePembayaran::all()->count();
        $no = $start+1;
        foreach($mp as $key => $value)
        {
            $value->no = $no;
            $no+=1;
        }
        $mp_json->data = $mp;
        return json_encode($mp_json);
    }
}
