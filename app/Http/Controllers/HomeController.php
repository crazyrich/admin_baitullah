<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use App\Travel;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $data = Admin::with('departmento')->find(Auth::id());
        // return $data;
        return view('dashboard.index');
    }

    public function addAdminTravel()
    {
        $travel = Travel::where("kode_travel","!=","SUPERADMIN")->get();
        return view('admin.create-admin', ["travels"=>$travel]);
    }

    public function createAdminTravel(Request $request)
    {
        $request->validate([
            'kode_travel' => 'required',
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'password' => 'required|confirmed|min:6',
        ]);
        $data_admin = $request->all();
        $data_admin["password"] = Hash::make($request->get('password'));
        $data_admin["created"] = Auth::user()->name;
        try {
            $admin = Admin::create($data_admin);
            return redirect()->back()->with('success','Berhasil membuat Admin baru. Email: '.$admin->email);
        } catch (\Exception $e) {
            return redirect()->back()->with('error','Gagal membuat Admin. error: '.$e->getMessage());
        }
    }

    public function daftarAdminTravel()
    {
        $daftar_travel = Travel::where("kode_travel","!=","SUPERADMIN")->get();
        return view('admin.daftar-admin', ["travels"=>$daftar_travel]);
    }

    public function getAdminTravel(Request $request)
    {
        $travel_id = $request->get("travel");
        if($travel_id == "all") {
            $admin = Admin::where("kode_travel","!=","SUPERADMIN")->get();
            $new_admin = array();
            foreach($admin as $key => $value)
            {
                $admin_travel = Travel::where("kode_travel","=", $value->kode_travel)->first();
                $value->nama_travel = $admin_travel->nama_travel;
                $value->icon_travel = $admin_travel->icon;
                array_push($new_admin, $value);
            }
            return $new_admin;
        } else {
            $admin = Admin::where("kode_travel","!=","SUPERADMIN")->where("kode_travel","=",$travel_id)->get();
            $new_admin = array();
            foreach($admin as $key => $value)
            {
                $admin_travel = Travel::where("kode_travel","=", $value->kode_travel)->first();
                $value->nama_travel = $admin_travel->nama_travel;
                $value->icon_travel = $admin_travel->icon;
                array_push($new_admin, $value);
            }
            return $new_admin;
        }
    }

    public function deleteAdmin(Request $request)
    {
        $admin = Admin::where('id',$request->get('id_admin'))->delete();
        return redirect()->back()->with('error','Admin berhasil di hapus');
    }

    public function editAdmin(Request $request)
    {
        $admin = Admin::where('id',$request->get('id_admin'))->first();
        $admin_travel = Travel::where("kode_travel","=", $admin->kode_travel)->first();
        return view('admin.edit-admin', ['admins'=>$admin, 'travels'=>$admin_travel]);
    }

    public function updateAdmin(Request $request)
    {
        $daftar_travel = Travel::where("kode_travel","!=","SUPERADMIN")->get();
        $request->validate([
            'nama_travel' => 'required',
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'password' => 'required|confirmed|min:6',
            'super_password_confirmation' => 'required'
        ]);
        if ($request->get('super_password_confirmation') == "superadmin123") {
            $admin_travel = Admin::find($request->get('id_admin'));
            $admin_travel->name = $request->get('name');
            $admin_travel->email = $request->get('email');
            $admin_travel->phone = $request->get('phone');
            $admin_travel->password = Hash::make($request->get('password'));
            $admin_travel->save();
            return redirect()->route('daftar-admin', ["travels"=>$daftar_travel])->with('success', 'Admin berhasil diubah');
        } else {
            return redirect()->route('daftar-admin', ["travels"=>$daftar_travel])->with('error', 'Gagal merubah Admin. Password tidak tepat');
        }
    }

    public function getKota()
    {
        $kota = DB::table('regencies')->get();
        return $kota;
    }

    public function getPropinsi()
    {
        $prop = DB::table('provinces')->get();
        return $prop;
    }

    public function getKotaPropinsi($propinsi)
    {
        $kota = DB::table('regencies')->where('province_id', $propinsi)->get();
        return $kota;
    }
}
