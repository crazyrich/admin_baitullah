<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Paket;
use App\Travel;
use App\DokumenPaket;
use DB;
use Auth;

class SiapBerangkat extends Controller
{
    public function index()
    {
        return view('siap_berangkat.index');
    }
    public function siap(Request $request)
    {
        $id_travel = $request->user()->kode_travel;
        $travel = Travel::where('kode_travel', $id_travel)->first();
        $start = $request->get('start');
        $length = $request->get('length');
        $paket_json = new \stdClass();
        $paket_json->draw = $request->get('draw');
        $paket = Paket::select('id_paket','nama_paket', 'tgl_keberangkatan', 'berangkat_dari', 'regencies.name as kota')
                        ->join('regencies','tabel_paket.berangkat_dari','regencies.id')
                        ->where('id_travel', $travel->id_travel)->orderBy('tgl_keberangkatan', 'ASC')->get();
        $paket_json->recordsTotal = Paket::where('id_travel', $travel->id_travel)->get()->count();
        $paket_json->recordsFiltered = Paket::where('id_travel', $travel->id_travel)->get()->count();
        $no = $start+1;
        foreach($paket as $key => $value)
        {
            $value->no = $no;
            $no+=1;
        }
        foreach($paket as $key => $value)
        {
            $jemaah = DB::table('tabel_checkout')->where('id_paket', $value->id_paket)->get();
            $value->jemaah = $jemaah->count();
        }
        $paket_json->data = $paket;
        return json_encode($paket_json);
    }

    public function dokumenJemaah(Request $request)
    {
        $start = $request->get('start');
        $length = $request->get('length');
        $id_paket = $request->get('id_paket');
        $get_jemaah = DB::table('tabel_checkout')->where('id_paket', $id_paket)->get();
        $dokumen_paket = array();
        foreach($get_jemaah as $key => $id_jemaah)
        {
            $data_jemaah = DB::table('user_data')->where('id', $id_jemaah->id_user_data)->select('id','nama_lengkap','no_hp', 'alamat_email')->first();
            $data_dokumen = DB::table('tabel_dokumen_jemaah')->where('id_jemaah', $id_jemaah->id_user_data)
                                    ->join("master_dokumen", "tabel_dokumen_jemaah.id_dokumen", "master_dokumen.id")
                                    ->select("master_dokumen.dokumen", "tabel_dokumen_jemaah.status")
                                    ->get();
            $data_jemaah->dokumen = $data_dokumen;
            array_push($dokumen_paket, $data_jemaah);
        }
        $jemaah_json = new \stdClass();
        $jemaah_json->draw = $request->get('draw');
        $jemaah_json->recordsTotal = $get_jemaah->count();
        $jemaah_json->recordsFiltered = $get_jemaah->count();
        $no = $start+1;
        foreach($dokumen_paket as $key => $value)
        {
            $value->no = $no;
            $no+=1;
        }
        $jemaah_json->data = $dokumen_paket;
        return json_encode($jemaah_json);
    }

    public function jemaah(Request $request)
    {
        $id_jemaah = $request->get('id');
        $dokumen = DB::table('tabel_dokumen_jemaah')->join('master_dokumen', 'tabel_dokumen_jemaah.id_dokumen','master_dokumen.id')
                        ->select('tabel_dokumen_jemaah.*','master_dokumen.dokumen')->where('id_jemaah', $id_jemaah)->get();
        $jemaah = DB::table('user_data')->where('id', $id_jemaah)->first();
        return view('dokumen.dokumen_jemaah', ['dokumen'=>$dokumen, 'jemaah'=>$jemaah]);
    }
}
