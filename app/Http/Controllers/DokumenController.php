<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dokumen;

class DokumenController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function save(Request $request)
    {
        $data = $request->all();
        $data["author"] = $request->user()->email;
        $dokumen = Dokumen::create($data);
        return redirect()->back()->with("success", "Dokumen berhasil ditambah");
    }
    public function daftarJson(Request $request)
    {
        $start = $request->get('start');
        $length = $request->get('length');
        $dok_json = new \stdClass();
        $dok_json->draw = $request->get('draw');
        $order_props = $request->get('order');
        $order_col = $order_props[0]['column'];
        $order_dir = $order_props[0]['dir'];
        if ($request->get('columns')[$order_col]["data"] == "no") {
            $dok = Dokumen::skip($start)->take($length)->get();
        } else {
            $dok = Dokumen::orderBy($request->get('columns')[$order_col]["data"], $order_dir)->skip($start)->take($length)->get();
        }
        $dok_json->recordsTotal = Dokumen::all()->count();
        $dok_json->recordsFiltered = Dokumen::all()->count();
        $no = $start+1;
        foreach($dok as $key => $value)
        {
            $value->no = $no;
            $no+=1;
        }
        $dok_json->data = $dok;
        return json_encode($dok_json);
    }
    public function delete(Request $request)
    {
        $id = $request->get('id');
        $dokumen = Dokumen::destroy($id);
        return redirect()->back()->with("error", "Dokumen berhasil dihapus");
    }
    public function getDokumen()
    {
        $dokumen = Dokumen::all();
        return $dokumen;
    }

    public function dokumenJemaahSimpan(Request $request)
    {
        
    }
}
