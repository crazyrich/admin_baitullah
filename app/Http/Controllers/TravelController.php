<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Travel;
use Validator;
use Auth;

class TravelController extends Controller
{
    public function __construct()
    {
        $this->url = env('APP_URL')."/icon/";
    }
    public function index()
    {
        $date = new \DateTime();
        $kode_trv = "TRV".$date->getTimestamp();
        return view('travel.create', ['kode_travel'=>$kode_trv]);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'kode_travel' => 'required',
            'nama_travel' => 'required',
            'lokasi' => 'required',
            'email' => 'required',
            'no_telepon' => 'required',
            'kota' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->route('home-paket')->with('error',$validator->errors());
        }
        $travel_data = $request->all();
        $icon = $request->file('icon-travel');
        $travel_data["author"] = Auth::user()->name;
        $travel_data["updater"] = Auth::user()->name;
        $travel_data["created"] = date('Y-m-d h:i:s');
        $travel_data["updated"] = date('Y-m-d h:i:s');
        $imageName = time().'.'.$icon->getClientOriginalExtension();
        $travel_data["icon"] = $imageName;
        $request->file('icon-travel')->move(public_path('icon'), $imageName);
        $intravel_dataput["status"] = 1;
        try {
            $travel = Travel::create($travel_data);
            return redirect()->route('home-travel')->with('success','Travel berhasil dibuat');
        } catch (\Exception $e) {
            return redirect()->route('home-travel')->with('error',$e->getMessage());
        }
    }

    public function daftar()
    {
        return view('travel.daftar');
    }

    public function daftarTravel(Request $request)
    {
        $start = $request->get('start');
        $length = $request->get('length');
        $travel_json = new \stdClass();
        $travel_json->draw = $request->get('draw');
        $order_props = $request->get('order');
        $order_col = $order_props[0]['column'];
        $order_dir = $order_props[0]['dir'];
        if ($request->get('columns')[$order_col]["data"] == "no") {
            $travel = Travel::where('kode_travel','!=','SUPERADMIN')->join('regencies','tabel_travel.kota','regencies.id')->skip($start)->take($length)->select('tabel_travel.*','regencies.name as nama_lokasi')->get();
        } else {
            $travel = Travel::where('kode_travel','!=','SUPERADMIN')->orderBy($request->get('columns')[$order_col]["data"], $order_dir)->skip($start)->take($length)->get();
        }
        $travel_json->recordsTotal = Travel::where('kode_travel','!=','SUPERADMIN')->get()->count();
        $travel_json->recordsFiltered = Travel::where('kode_travel','!=','SUPERADMIN')->get()->count();
        $no = $start+1;
        foreach($travel as $key => $value)
        {
            $value->no = $no;
            $no+=1;
        }
        $travel_json->data = $travel;
        return json_encode($travel_json);
    }

    public function deleteTravel(Request $request)
    {
        $id = $request->get('id_admin');
        $travel = Travel::where('id_travel','=',$id)->delete();
        return redirect()->back()->with('error','Travel berhasil dihapus');
    }

    public function editTravel(Request $request)
    {
        $travel = Travel::where('id_travel', '=', $request->get('id_admin'))->first();
        return view('travel.edit', ['travel'=>$travel]);
    }

    public function updateTravel(Request $request)
    {
        $check_img = $request->file('icon-travel');
        if (!$check_img) {
            $param = $request->except('icon-travel');
            $travel = Travel::where('kode_travel', '=', $param["kode_travel"])->update(
                [
                    'nama_travel'=> $param["nama_travel"],
                    'no_telepon'=> $param["no_telepon"],
                    'no_hp'=> $param["no_hp"],
                    'email'=> $param["email"],
                    'lokasi'=> $param["lokasi"],
                ]
            );
            return redirect()->route('home-travel')->with('success','Travel '.$request->nama_travel.' berhasil diperbarui');
        } else {
            $param = $request->except('icon-travel');
            $icon = $request->file('icon-travel');
            $imageName = time().'.'.$icon->getClientOriginalExtension();
            $icon->move(public_path('icon'), $imageName);
            $travel = Travel::where('kode_travel', '=', $param["kode_travel"])->update(
                [
                    'nama_travel'=> $param["nama_travel"],
                    'no_telepon'=> $param["no_telepon"],
                    'no_hp'=> $param["no_hp"],
                    'email'=> $param["email"],
                    'lokasi'=> $param["lokasi"],
                    'icon'=> $this->url.$imageName,
                ]
            );
            return redirect()->route('home-travel')->with('success','Travel '.$request->nama_travel.' berhasil diperbarui');
        }
    }
}
