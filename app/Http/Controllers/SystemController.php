<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Travel;
use App\Paket;
use DB;

class SystemController extends Controller
{

    public function checkEmail(Request $request, $jenis)
    {
        $params = $request->get('params');
        $users = DB::table('user_data')->where($jenis, $params)->get()->count();
        return $users;
    }
}
