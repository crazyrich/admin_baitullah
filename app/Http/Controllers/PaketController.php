<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Travel;
use App\Paket;
use App\Maskapai;
use App\Fasilitas;
use App\GambarPaket;
use App\Kategori;
use App\HargaPaket;
use App\RuteMaskapai;
use App\Hotel;
use App\Itenary;
use App\Dokumen;
use App\DokumenPaket;
use DB;

class PaketController extends Controller
{
    public function __construct()
    {
        $this->url = env('APP_URL');
    }
    public function index()
    {
        $travel = Travel::where('kode_travel','=', Auth::user()->kode_travel)->first();
        $maskapai = Maskapai::get();
        $kategori_paket = Kategori::where('author','like','%SUPERADMIN%')->orWhere('author','like','%'.Auth::user()->kode_travel.'%')->get();
        $hotel = Hotel::select('lokasi')->groupBy('lokasi')->get();
        $dokumen = Dokumen::all();
        $kota = DB::table('regencies')->get();
        return view('paket.create', ["travel"=>$travel, "maskapai"=>$maskapai, "kategori"=>$kategori_paket, "hotel"=>$hotel, "dokumen"=>$dokumen, "kota"=>$kota]);
    }

    public function create(Request $request)
    {
        // return $request->all();
        $dt = new \DateTime();
        $paket_param = $request->all();
        $paket_param["id_travel"] = Auth::user()->travel["id_travel"];
        $paket_param["tgl_keberangkatan"] = date('Y-m-d',strtotime($request->get('tgl_keberangkatan')));
        $paket_param["id_kategori"] = $request->get('kategori');
        $paket_param["author"] = Auth::user()->email;
        $paket_param["updater"] = Auth::user()->email;
        $paket_param["created"] = $dt->format('Y-m-d H:i:s');
        $paket_param["updated"] = $dt->format('Y-m-d H:i:s');
        $paket = Paket::create($paket_param);
        $maskapai = $request->get('perjalanan');
        foreach($maskapai as $maskapai_key => $maskapai_val)
        {
            $param_maskapai = array();
            $param_maskapai["id_paket"] = $paket["id"];
            $param_maskapai["maskapai"] = $request->get('maskapai')[$maskapai_key];
            $param_maskapai["dari"] = $request->get('bandara-awal')[$maskapai_key];
            $param_maskapai["ke"] = $request->get('bandara-tujuan')[$maskapai_key];
            $param_maskapai["pemberangkatan"] = $maskapai_val;
            $param_maskapai["author"] = Auth::user()->email;
            $param_maskapai["updater"] = Auth::user()->email;
            $result_maskapai = RuteMaskapai::create($param_maskapai);
        }
        $get_fasilitas = join(';', $request->get('termasuk'));
        $get_t_fasilitas = join(';', $request->get('t_termasuk'));
        $tipe_kamar = $request->get('tipe_kamar');
        $harga_kamar = $request->get('harga_kamar');
        for($i = 0;$i<count($request->get('tipe_kamar'));$i++) {
            $harga_paket = array();
            $tipe = $tipe_kamar[$i];
            $harga = $harga_kamar[$i];
            $harga_paket["harga"] = $harga;
            $harga_paket["jenis"] = $tipe;
            $harga_paket["id_paket"] = $paket["id"];
            $harga_paket["id_kategori_harga"] = $request->get('kategori');
            $harga_paket["author"] = Auth::user()->email;
            $harga_paket["updater"] = Auth::user()->email;
            $harga_paket["status"] = 1;
            $harga_paket["created"] = $dt->format('Y-m-d H:i:s');
            $harga_paket["updated"] = $dt->format('Y-m-d H:i:s');
            $result_harga = HargaPaket::create($harga_paket);
        }
        $fasilitas = array();
        $fasilitas["include"] = $get_fasilitas;
        $fasilitas["n_include"] = $get_t_fasilitas;
        $fasilitas["id_paket"] = $paket["id"];
        $fasilitas["author"] = Auth::user()->email;
        $fasilitas["updater"] = Auth::user()->email;
        $result_fasilitas = Fasilitas::create($fasilitas);
        foreach($request->get('hari') as $key => $value) {
            $itenary = array();
            $itenary["id_paket"] = $paket["id"];
            $itenary["hari"] = $value;
            $itenary["kegiatan"] = $request->get("kegiatan")[$key];
            $itenary_result = Itenary::create($itenary);
        }
        foreach($request->get('dokumen') as $key => $value) {
            $dokumen = array();
            $dokumen["id_paket"] = $paket["id"];
            $dokumen["id_dokumen"] = $value;
            $dokumen["catatan"] = $request->get("dok_catatan")[$key];
            $dokumen["author"] = Auth::user()->email;
            $dokumen["updater"] = Auth::user()->email;
            $result_dokumen = DokumenPaket::create($dokumen);
        }
        foreach($request->get('hotel') as $key=>$value)
        {
            DB::table('tabel_paket_hotel')->insert(["id_paket"=>$paket["id"], "id_hotel"=>$value, "lokasi"=>$request->get('lokasi')[$key]]);
        }
        $sk = DB::table('tabel_sk')->insert(["id_paket"=>$paket["id"], "sk"=>$request->get('sk'), 
                        "author"=>$request->user()->email, "updater"=>$request->user()->email, "created_at"=>date('Y-m-d H:i:s'), "updated_at"=>date('Y-m-d H:i:s')]);
        $order_gambar = 1;
        foreach($request->file('gambar_paket') as $gambar) {
            $imageName = time().$order_gambar.'.'.$gambar->getClientOriginalExtension();
            $gambar->move(public_path('images'), $imageName);
            $gambar_paket = array();
            $gambar_paket["id_paket"] = $paket["id"];
            $gambar_paket["gambar"] = "/images/".$imageName;
            $gambar_paket["display_order"] = $order_gambar;
            $gambar_paket["author"] = Auth::user()->email;
            $gambar_paket["updater"] = Auth::user()->email;
            $dt = new \DateTime();
            $gambar_paket["created"] = $dt->format('Y-m-d H:i:s');
            $gambar_paket["updated"] = $dt->format('Y-m-d H:i:s');
            $order_gambar++;
            $save_gbr = GambarPaket::create($gambar_paket);
        }
        return redirect()->route('home-paket')->with('success','Paket berhasil dibuat');
    }

    public function daftar()
    {
        $travel = Travel::where('kode_travel','=', Auth::user()->kode_travel)->first();
        $data = Paket::where("id_travel","=", $travel->id_travel)->get();
        return view('paket.daftar', ['pakets'=>$data]);
    }

    public function daftarJson(Request $request)
    {
        $start = $request->get('start');
        $length = $request->get('length');
        $paket_json = new \stdClass();
        $paket_json->draw = $request->get('draw');
        $order_props = $request->get('order');
        $order_col = $order_props[0]['column'];
        $order_dir = $order_props[0]['dir'];
        if ($request->get('columns')[$order_col]["data"] == "no") {
            $paket = Paket::where('id_travel','=', $request->user()->travel->id_travel)->skip($start)->take($length)->get();
        } else {
            $paket = Paket::where('id_travel','=', $request->user()->travel->id_travel)->orderBy($request->get('columns')[$order_col]["data"], $order_dir)->skip($start)->take($length)->get();
        }
        $paket_json->recordsTotal = Paket::all()->count();
        $paket_json->recordsFiltered = Paket::all()->count();
        $no = $start+1;
        foreach($paket as $key => $value)
        {
            $kategori = Kategori::where('id_kategori_paket', '=', $value->id_kategori)->first();
            $value->no = $no;
            $value->kategori = $kategori->kategori_paket;
            $no+=1;
        }
        $paket_json->data = $paket;
        return json_encode($paket_json);
    }

    public function delete(Request $request)
    {
        $id = $request->get('id_admin');
        $maskapai = Paket::where('id_paket','=',$id)->delete();
        return redirect()->back()->with('error','Paket berhasil dihapus');
    }

    public function edit($id)
    {
        $paket = Paket::join('regencies','tabel_paket.berangkat_dari', 'regencies.id')->where('id_paket', $id)->select('tabel_paket.*','regencies.name as berangkat')->first();
        $kategori = Kategori::all();
        $travel = Travel::where('id_travel', $paket->id_travel)->first();
        $hotel_paket = DB::table('tabel_paket_hotel')->join('hotel','tabel_paket_hotel.id_hotel','hotel.id')->select('tabel_paket_hotel.id','tabel_paket_hotel.id_hotel','tabel_paket_hotel.lokasi', 'hotel.name')->where('id_paket', $id)->get();
        $maskapai = Maskapai::all();
        $perjalanan = RuteMaskapai::where('id_paket',$id)->get();
        $fasilitas = Fasilitas::where('id_paket',$id)->first();
        $gambar = GambarPaket::where('id_paket',$id)->orderBy('display_order', 'ASC')->get();
        $itenary = Itenary::where('id_paket',$id)->orderBy('hari', 'ASC')->get();
        $sk = DB::table('tabel_sk')->where('id_paket', $id)->first();
        $dokumen_has = DokumenPaket::where('id_paket',$id)->join('master_dokumen', 'tabel_dokumen_paket.id_dokumen', 'master_dokumen.id')->select('master_dokumen.dokumen AS dokumen', 'tabel_dokumen_paket.catatan AS dokumen_catatan','tabel_dokumen_paket.id')->get();
        $dokumen = Dokumen::all();
        $hotel = Hotel::select('lokasi')->groupBy('lokasi')->get();
        $list_hotel = Hotel::all();
        return view('paket.edit', ['paket'=>$paket, 'kategori'=>$kategori, 'travel'=>$travel, 'hotel'=>$hotel_paket, 'maskapai'=>$maskapai, 
        'rute'=>$perjalanan, 'fasilitas'=>$fasilitas, 'gambar'=>$gambar, 'itenary'=>$itenary, 'sk'=>$sk, 'dokumen'=>$dokumen_has, "new_dok"=>$dokumen, "hotel_list"=>$hotel, "hotels"=>$list_hotel]);
    }

    public function updateDetail(Request $request)
    {
        $request->validate([
            "id_travel"=>"required",
            "nama_paket" => "required",
            "id_kategori" => "required",
            "tgl_keberangkatan" => "required",
            "lama_perjalanan" => "required",
            "berangkat_dari" => "required"
        ]);
        $data = $request->only("id_travel","nama_paket","id_kategori","tgl_keberangkatan","lama_perjalanan","id_hotel","berangkat_dari");
        $data["updated"] = date('Y-m-d H:i:s');
        $data["updater"] = $request->user()->name;
        try {
            $paket = Paket::where("id_paket",$request->id_paket)->update($data);
            return redirect()->route('daftar-paket')->with('success','Detail paket berhasil diubah');
        } catch (\Exception $e) {
            return redirect()->back()->withError($e->getMessage())->withInput();
        }

    }

    public function updateRute(Request $request)
    {
        RuteMaskapai::where('id',$request->get('id'))->update([
                            "maskapai"=>$request->get('maskapai'), 
                            "dari"=>$request->get('dari'),
                            "ke"=>$request->get('ke'),
                            "updated_at"=>date('Y-m-d H:i:s'),
                            "updater"=>$request->user()->name]);
        return redirect()->route('daftar-paket')->with('success','Rute perjalanan '.$request->get('nama_paket').' berhasil diubah');
    }

    public function updateGambar(Request $request)
    {
        $i = 0;
        $display_oder = $request->get('display_order');
        $id_paket = $request->get('id_paket');
        // GambarPaket::where('id_paket', $id_paket)->delete();
        if($request->file('gambar-paket')) {
            foreach($request->file('gambar-paket') as $key => $gambar) {
                $imageName = time().'.'.$gambar->getClientOriginalExtension();
                $gambar->move(public_path('images'), $imageName);
                $gambar_paket = array();
                $gambar_paket["id_paket"] = $id_paket;
                $gambar_paket["gambar"] = "/images/".$imageName;
                $gambar_paket["display_order"] = $display_oder[$key];
                $gambar_paket["author"] = Auth::user()->name;
                $gambar_paket["updater"] = Auth::user()->name;
                $dt = new \DateTime();
                $gambar_paket["created"] = $dt->format('Y-m-d H:i:s');
                $gambar_paket["updated"] = $dt->format('Y-m-d H:i:s');
                $i++;
                $save_gbr = GambarPaket::create($gambar_paket);
            }
        }
        if ($request->file('old-gambar-paket')) {
            foreach($request->file('old-gambar-paket') as $key => $old_gambar) {
                $imageName = time().'.'.$old_gambar->getClientOriginalExtension();
                $old_gambar->move(public_path('images'), $imageName);
                $dt = new \DateTime();
                $save_gbr = GambarPaket::where('id_gambar_paket',$request->get('id-gambar-paket')[$key])->update([
                    "gambar"=> "/images/".$imageName,
                    "display_order"=> $request->get('old-display-order')[$key],
                    "updater"=>$request->user()->name,
                    "updated"=>$dt->format('Y-m-d H:i:s')
                ]);
            }
        }
        if ($request->get('old-display-order')){
            foreach($request->get('old-display-order') as $key => $old_display) {
                $save_gbr = GambarPaket::where('id_gambar_paket',$request->get('id-gambar-paket')[$key])->update([
                    "display_order"=> $request->get('old-display-order')[$key],
                    "updater"=>$request->user()->name,
                    "updated"=>date('Y-m-d H:i:s')
                ]);
            }
        }
        return redirect()->back()->with('success','Gambar paket berhasil diperbarui');
    }

    public function updateFasilitas(Request $request)
    {
        $include = join(';', $request->get('include'));
        $n_include = join(';', $request->get('n_include'));
        Fasilitas::where('id_paket', $request->get('id_paket'))->update(["include"=>$include, "n_include"=>$n_include]);
        return redirect()->route('daftar-paket')->with('success','Fasilitas '.$request->get('nama_paket').' berhasil diperbarui');
    }

    public function updateItenary(Request $request)
    {
        if($request->get('hari')) {
            foreach($request->get('hari') as $key => $value) {
                Itenary::where('id', $request->get('old_id_itenary')[$key])->update(["hari"=>$value,"kegiatan"=>$request->get('kegiatan')[$key]]);
            }
        }
        if($request->get('hari-new')) {
            foreach($request->get('hari-new') as $key => $value) {
                $itenary = array();
                $itenary["id_paket"] = $request->get('id_paket');
                $itenary["hari"] = $value;
                $itenary["kegiatan"] = $request->get("kegiatan-new")[$key];
                $itenary_result = Itenary::create($itenary);
            }
        }
        return redirect()->back()->with('success','Itenary berhasil diperbarui');
    }

    public function updateSK(Request $request)
    {
        DB::table('tabel_sk')->where('id_paket', $request->get('id_paket'))->update(["sk"=>$request->get('sk'), "updater"=>$request->user()->name, "updated_at"=>date('Y-m-d')]);
        return redirect()->back()->with('success','Syarat & Ketentuan berhasil diperbarui');
    }

    public function updateDokumen(Request $request)
    {
        
    }

    public function listHotel($lokasi)
    {
        $hotel = Hotel::where('lokasi', $lokasi)->get();
        return $hotel;
    }

    public function deleteGambar(Request $request)
    {
        $id = $request->get('hapus-id-gambar');
        $id_paket = $request->get('id_paket');
        $gambar = GambarPaket::where('id_paket', $id_paket)->get()->count();
        if ($gambar < 2)
        {
            return redirect()->back()->with('error', 'Gagal hapus gambar. Paket harus memiliki minimal 1 gambar');    
        } else {
            GambarPaket::where('id_gambar_paket', $id)->delete();
        }
        return redirect()->back()->with('error', 'Gambar berhasil dihapus');
    }

    public function deleteHotel(Request $request)
    {
        $id = $request->get('hapus-id-hotel');
        $id_paket = $request->get('id_paket');
        $hotel = DB::table('tabel_paket_hotel')->where('id_paket', $id_paket)->get()->count();
        if ($hotel < 3)
        {
            return redirect()->back()->with('error', 'Gagal hapus hotel. Paket harus memiliki minimal 2 hotel');    
        } else {
            DB::table('tabel_paket_hotel')->where('id', $id)->delete();
        }
        return redirect()->back()->with('error', 'Hotel berhasil dihapus');
    }

    public function deleteItenary(Request $request)
    {
        $id = $request->get('hapus-id-itenary');
        $id_paket = $request->get('id_paket');
        Itenary::where('id', $id)->delete();
        return redirect()->back()->with('error', 'Itenary berhasil dihapus');
    }

    public function tambahHotel(Request $request)
    {
        $id_paket = $request->get('id_paket');
        foreach($request->get('hotel') as $key=>$value)
        {
            DB::table('tabel_paket_hotel')->insert(["id_paket"=>$id_paket, "id_hotel"=>$value, "lokasi"=>$request->get('lokasi')[$key]]);
        }
        return redirect()->back()->with('success', 'Hotel berhasil ditambah');
    }

    public function deleteDokumen(Request $request)
    {
        $id = $request->get('id');
        DB::table('tabel_dokumen_paket')->where('id', $id)->delete();
        return redirect()->back()->with('error', 'Dokumen berhasil dihapus');
    }

    public function tambahDokumen(Request $request)
    {
        $paket_id = $request->get('id_paket');
        foreach($request->get('dokumen') as $key => $value) {
            $dokumen = array();
            $dokumen["id_paket"] = $paket_id;
            $dokumen["id_dokumen"] = $value;
            $dokumen["catatan"] = $request->get("dok_catatan")[$key];
            $dokumen["author"] = $request->user()->email;
            $dokumen["updater"] = $request->user()->email;
            $result_dokumen = DokumenPaket::create($dokumen);
        }
        return redirect()->back()->with('success', 'Dokumen berhasil ditambahkan');
    }

    public function getPaket(Request $request)
    {
        $id_travel = $request->user()->travel->id_travel;
        $list_paket = Paket::where('id_travel', $id_travel)->where('id_kategori', $request->jenis_paket)->get();
        return $list_paket;
    }

    public function getPaketDetail(Request $request)
    {
        $id_paket = $request->get('id_paket');
        $id_travel = $request->user()->travel->id_travel;
        $list_paket = Paket::where('id_paket', $id_paket)
                    ->join('tabel_kategori_paket', 'tabel_paket.id_kategori', 'tabel_kategori_paket.id_kategori_paket')
                    ->select('tabel_paket.*','tabel_kategori_paket.kategori_paket')
                    ->first();
        return $list_paket;
    }

    public function getTipeKamar(Request $request)
    {
        $id_paket = $request->get('id_paket');
        $harga_paket = DB::table('trs_harga_paket')->where('id_paket', $id_paket)->select('id_harga_paket','harga','jenis')->get();
        return $harga_paket;
    }

    public function getFasilitas(Request $request)
    {
        $id_paket = $request->get('id_paket');
        $fasilitas = Fasilitas::where('id_paket', $id_paket)->first();
        return $fasilitas;
    }

    public function getDokumen(Request $request)
    {
        $id_paket = $request->get('id_paket');
        $dokumen = DokumenPaket::join('master_dokumen', 'tabel_dokumen_paket.id_dokumen', 'master_dokumen.id')->where('id_paket', $id_paket)->select('tabel_dokumen_paket.catatan','master_dokumen.dokumen')->get();
        return $dokumen;
    }

    public function getMaskapai(Request $request)
    {
        $id_paket = $request->get('id_paket');
        $maskapai = RuteMaskapai::where('id_paket', $id_paket)->join('tabel_maskapai', 'tabel_rute_maskapai.maskapai', 'tabel_maskapai.id')
                    ->select('tabel_maskapai.nama', 'tabel_rute_maskapai.dari', 'tabel_rute_maskapai.ke', 'tabel_maskapai.icon')->get();
        return $maskapai;
    }
}
