<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paket extends Model
{
    protected $table = "tabel_paket";

    protected $fillable = [
        'id_travel', 'id_kategori', 'nama_paket', 'tgl_keberangkatan', 'lama_perjalanan', 
        'berangkat_dari', 'created', 'author', 'updated', 'updater', 'status'
    ];

    public $timestamps = false;

    public function travel() {
        return $this->hasOne(Travel::class);
    }

    public function kategori() {
        return $this->hasOne(Kategori::class);
    }

    public function maskapai() {
        return $this->hasOne(Maskapai::class);
    }

    public function fasilitas() {
        return $this->hasMany(Fasilitas::class);
    }

    public function gambar() {
        return $this->hasMany(GambarPaket::class);
    }

    public function harga() {
        return $this->hasMany(HargaPaket::class);
    }
}
