<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Travel extends Model
{
    protected $table = "tabel_travel";
    public $primaryKey = "id_travel";
    protected $fillable = [
        'kode_travel', 'nama_travel', 'lokasi', 'maps', 'email', 'no_telepon', 'no_hp', 
        'icon', 'created', 'author', 'updated', 'updater', 'status', 'kota'
    ];

    public $timestamps = false;

    public function paket()
    {
        $this->hasMany(Paket::class, 'id_travel', 'id_travel');
    }

}
