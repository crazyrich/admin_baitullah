<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Itenary extends Model
{
    protected $table = "tabel_itenary";
    protected $fillable = [
        'id_paket', 'hari', 'kegiatan'
    ];
    public $timestamps = false;
}
