<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HargaPaket extends Model
{
    protected $table = "trs_harga_paket";

    protected $fillable = [
        'id_paket', 'id_kategori_harga', 'harga', 'jenis', 'created', 'author', 'updated', 'updater', 'status'
    ];

    public $timestamps = false;
}
