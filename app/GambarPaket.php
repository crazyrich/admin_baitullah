<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GambarPaket extends Model
{
    protected $table = "trs_gambar_paket";

    protected $fillable = [
        'id_paket', 'gambar', 'display_order', 'created', 'author', 'updated', 'updater', 'status'
    ];

    public $timestamps = false;
}
