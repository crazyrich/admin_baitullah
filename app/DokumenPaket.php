<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DokumenPaket extends Model
{
    public $table = "tabel_dokumen_paket";
    protected $fillable = ["id_paket", "id_dokumen", "catatan", "author", "updater"];
}
