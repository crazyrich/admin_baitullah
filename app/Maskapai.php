<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Maskapai extends Model
{
    protected $table = "tabel_maskapai";

    protected $fillable = [
        'kode_maskapai','nama', 'alamat', 'no_tlp', 'no_fax', 'website', 'creator', 'updater', 'icon'
    ];

}
