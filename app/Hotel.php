<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    protected $table = "hotel";

    protected $fillable = [
        'name', 'alamat', 'bintang', 'no_tlp', 'website', 'icon', 'lokasi'
    ];
}
