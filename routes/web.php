<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::group(['middleware' => ['auth']], function(){
	Route::get('/register', function() {
        return view('auth.register');
    })->name('register');
    Route::get('/', function () {
        return view('admin/dashboard');
    });
    Route::get('get/kota/all', 'HomeController@getKota');
    Route::get('get/kota/{propinsi}', 'HomeController@getKotaPropinsi');
    Route::get('get/propinsi/all', 'HomeController@getPropinsi');
});

Route::group(['middleware' => ['auth'], 'prefix'=>'banners'], function(){
    Route::get('/', 'BannerController@index')->name('home-banner');
    Route::post('/save', 'BannerController@save');
    Route::get('/daftar/json', 'BannerController@daftar');
    Route::post('/delete', 'BannerController@delete');
    Route::post('/update', 'BannerController@update');
});

Route::group(['middleware' => ['auth'], 'prefix'=>'kategori'], function(){
    Route::get('/', 'KategoriController@index')->name('home-kategori');
    Route::post('/add', 'KategoriController@create');
    Route::post('/delete', 'KategoriController@delete');
    Route::get('/all', 'KategoriController@getAll');
    Route::get('/daftar', 'KategoriController@daftarKategori');
});

Route::group(['middleware' => ['auth'], 'prefix'=>'paket'], function(){
    Route::get('/', 'PaketController@index')->name('home-paket');
    Route::post('/update-gambar', 'PaketController@updateGambar');
    Route::post('/add', 'PaketController@create');
    Route::get('/daftar', 'PaketController@daftar')->name('daftar-paket');
    Route::get('/daftar/json', 'PaketController@daftarJson');
    Route::post('/delete', 'PaketController@delete');
    Route::get('/edit/{id}', 'PaketController@edit');
    Route::post('/detail/update', 'PaketController@updateDetail');
    Route::post('/rute/update', 'PaketController@updateRute');
    Route::post('/fasilitas/update', 'PaketController@updateFasilitas');
    Route::post('/itenary/update', 'PaketController@updateItenary');
    Route::post('/sk/update', 'PaketController@updateSK');
    Route::post('/dokumen/update', 'PaketController@updateDokumen');
    Route::post('/gambar/hapus', 'PaketController@deleteGambar');
    Route::post('/tambah/hotel', 'PaketController@tambahHotel');
    Route::post('/hotel/hapus', 'PaketController@deleteHotel');
    Route::post('/itenary/hapus', 'PaketController@deleteItenary');
    Route::post('/dokumen/hapus', 'PaketController@deleteDokumen');
    Route::post('/dokumen/create', 'PaketController@tambahDokumen');
    Route::group(['prefix' => 'daftar'], function () {
        Route::get('kamar', 'PaketController@getTipeKamar');
        Route::get('fasilitas', 'PaketController@getFasilitas');
        Route::get('dokumen', 'PaketController@getDokumen');
        Route::get('maskapai', 'PaketController@getMaskapai');
    });
});

Route::group(['middleware' => ['auth'], 'prefix'=>'travel'], function(){
    Route::get('/', 'TravelController@index')->name('home-travel');
    Route::post('/add', 'TravelController@create');
    Route::get('/admin', 'HomeController@addAdminTravel');
    Route::post('/create-admin', 'HomeController@createAdminTravel');
    Route::get('/daftar-admin', 'HomeController@daftarAdminTravel')->name('daftar-admin');
    Route::get('/get-list-admin', 'HomeController@getAdminTravel');
    Route::post('/delete-admin', 'HomeController@deleteAdmin');
    Route::get('/edit-admin', 'HomeController@editAdmin');
    Route::post('/update-admin', 'HomeController@updateAdmin');
    Route::get('/daftar', 'TravelController@daftar');
    Route::get('/daftar/json', 'TravelController@daftarTravel');
    Route::post('/delete', 'TravelController@deleteTravel');
    Route::post('/edit', 'TravelController@editTravel');
    Route::post('/update', 'TravelController@updateTravel');
});

Route::group(['middleware' => ['auth'], 'prefix'=>'maskapai'], function(){
    Route::get('/', 'MaskapaiController@index')->name('home-maskapai');
    Route::post('/add', 'MaskapaiController@create');
    Route::get('/daftar/json', 'MaskapaiController@daftar');
    Route::get('/daftar', 'MaskapaiController@all')->name('daftar-maskapai');
    Route::post('/delete', 'MaskapaiController@delete');
    Route::post('/edit', 'MaskapaiController@edit');
    Route::post('/update', 'MaskapaiController@update');
});

Route::group(['middleware' => ['auth'], 'prefix'=>'pesanan'], function(){
    Route::get('/daftar', 'PemesananController@getPemesanan');
    Route::get('/pembayaran/json', 'PemesananController@getProsesPemesananJson');
    Route::get('/belum-di-proses', 'PemesananController@belumDiProses');
    Route::get('/open/image', 'PemesananController@openImage');
    Route::post('/konfirmasi', 'PemesananController@konfirmasiPemesanan');
    Route::get('/all/json', 'PemesananController@allPemesananJson');
    Route::get('/all', 'PemesananController@allPemesanan');
    Route::get('/tambah', 'PemesananController@tambah');
    Route::post('/simpan', 'PemesananController@simpan');
});

Route::group(['middleware' => ['auth'], 'prefix'=>'hotel'], function(){
    Route::get('/daftar', 'HotelController@index')->name('home-hotel');
    Route::get('/daftar/json', 'HotelController@getHotel');
    Route::get('/buat', 'HotelController@buat');
    Route::post('/add', 'HotelController@create');
    Route::post('/delete', 'HotelController@delete');
    Route::post('/edit', 'HotelController@edit');
    Route::post('/update', 'HotelController@update');
    Route::get('/daftar/lokasi', 'HotelController@daftarLokasi');
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('/laravel-filemanager', '\UniSharp\LaravelFilemanager\Controllers\LfmController@show');
    Route::post('/laravel-filemanager/upload', '\UniSharp\LaravelFilemanager\Controllers\UploadController@upload');
    Route::get('/umrah', 'SiapBerangkat@index');
    Route::get('/umrah/ready/json', 'SiapBerangkat@siap');
    // list all lfm routes here...
});

Route::group(['middleware' => ['auth'], 'prefix' => 'metode-pembayaran'], function () {
    Route::get('/tambah', 'MetodePembayaranController@tambah');
    Route::post('/save', 'MetodePembayaranController@save');
    Route::get('/daftar', 'MetodePembayaranController@daftar');
    Route::get('/daftar/json', 'MetodePembayaranController@daftarJson');
});

Route::group(['middleware' => ['auth'], 'prefix' => 'dokumen'], function () {
    Route::get('/', function() {
        return view('dokumen.create');
    });
    Route::post('/save', 'DokumenController@save');
    Route::get('/daftar/json', 'DokumenController@daftarJson');
    Route::post('/delete', 'DokumenController@delete');
    Route::get('/jemaah/json', 'SiapBerangkat@dokumenJemaah');
    Route::get('all', 'DokumenController@getDokumen');
    Route::get('/jemaah', 'SiapBerangkat@jemaah');
    Route::get('/jemaah/simpan', 'DokumenController@dokumenJemaahSimpan');
});

Route::group(['middleware' => ['auth'], 'prefix' => 'system'], function () {
    Route::get('/get/paket', 'PaketController@getPaket');
    Route::get('/get/paket/detail', 'PaketController@getPaketDetail');
    Route::get('/check/{jenis}', 'SystemController@checkEmail');
});

Route::get('/login', function() {
    return view('admin.login');
})->name('login');
Route::get('/test', 'SiapBerangkat@dokumenJemaah');
// Route::post('/login', 'Auth\LoginController@userLogin');
Route::get('/home', 'HomeController@index')->name('home');
