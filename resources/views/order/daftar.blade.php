@extends('layouts.master')
@section('css-here')
    <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection
@section('content')
<section class="content-header">
    <h1>Pesanan<small> Setting</small></h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Kategori</li>
    </ol>
</section>
<section class="content">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="modal fade" id="modal-default">
            <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Bukti Pembayaran</h4>
                </div>
                <div class="modal-body">
                <img src="#" class="img-responsive" max-width="500px" id="bukti-img">
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Daftar Pesanan (Menunggu Konfirmasi)</h3>
                </div>
                <div class="box-body">
                    <table class="table table-bordered" id="pesanan-table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Pembayaran</th>
                                <th>Nama Paket</th>
                                <th>Pembeli</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="{{ asset('/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{asset('AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script>
    $('#daftar-order').addClass('active');
    $('#daftar-order').closest('li.treeview').addClass('menu-open');
    $('#daftar-order').closest('ul.treeview-menu').css('display', 'block');
    $('#pesanan-table').DataTable({
            "processing": true,
            "serverSide": true,
            "ordering": true,
            "ajax": "/pesanan/all/json",
            "autoWidth" : false,
            "fixedColumns": true,
            "scrollX": "300px",
            "columns": [
                { "data": "no", "orderable": false },
                { "data": "kode_pembayaran" },
                { "data": "paket" },
                { "data": "author" },
                { "data": "status_pembayaran" },
                { "data": "id", render: function(data, type, row){
                    var button_edit = "<button type='button' class='btn btn-success' data-toggle='tooltip' title='Edit' style='margin-right: 5px' onclick='openImage("+data+")'>Lihat Bukti</button>"
                    var button_delete = "<form action='konfirmasi' method='POST' style='margin: 0; padding: 0;margin-block-end: 0;display: inline !important'><input type='hidden' name='_token' value='{{ csrf_token() }}'><input type='hidden' name='id_admin' value='"+data+"'><button type='submit' class='btn btn-info' data-toggle='tooltip' title='Hapus'>Konfirmasi</button></form>"
                    return button_edit+button_delete
                } }
            ]
        })
    function openImage(id)
    {
        $.ajax({
            url : 'open/image',
            type: 'GET',
            data: {
                    'id' : id
                },
            success: function(response) {
                $('#bukti-img').attr('src',response["bukti_pembayaran"]);
                $('#modal-default').modal('show');
            }
        })
    }
</script>
@endsection