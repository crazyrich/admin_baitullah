@extends('layouts.master')
@section('css-here')
    <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <style>
        .paket-place tr td:first-child {
            width: 30%;
        }
    </style>
@endsection
@section('content')
<section class="content-header">
    <h1>Pesanan<small> Setting</small></h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Kategori</li>
    </ol>
</section>
<section class="content">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="modal fade" id="modal-default">
            <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Modal Title</h4>
                </div>
                <div class="modal-body">
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Pilih Paket</h3>
                        </div>
                        <div class="box-body">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="form-group">
                                        <label>Pilih Jenis Paket</label>
                                        <select name="jenis_paket" class="form-control">
                                            <option value="0" disabled selected>--Pilih Jenis Paket--</option>
                                            @foreach ($kategori as $item)
                                                <option value="{{ $item->id_kategori_paket }}">{{ $item->kategori_paket }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Pilih Paket</label>
                                        <select name="paket" class="form-control">
                                            <option value="0" disabled selected>--Pilih Paket--</option>
                                        </select>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Pendaftaran Jemaah</h3>
                        </div>
                        <div class="box-body">
                            <form action="{{ url('/pesanan/simpan') }}" method="POST">
                                @csrf
                                <input type="hidden" name="id_paket" required>
                                <input type="hidden" name="id_travel" required>
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input name="nama" type="text" class="form-control" placeholder="ex. Budiman" required>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6" style="padding-left: 0px !important">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input name="email" type="email" class="form-control" placeholder="ex. budiman@baitullah.co.od" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6" style="padding-left: 0px !important; padding-right: 0px !important">
                                            <div class="form-group">
                                                <label>No Telepon</label>
                                                <input name="phone" type="text" class="form-control" placeholder="ex. 0812272xxx" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Jenis Kelamin</label>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="gender" value="pria" required>
                                            Pria
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="gender" value="wanita">
                                            Wanita
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Tanggal lahir</label>
                                    <input name="tgl_lahir" type="text" class="form-control datepicker" autocomplete="off"required>
                                </div>
                                <div class="form-group">
                                    <label>Pilih Kamar</label>
                                    <select name="tipe-kamar" class="form-control" required>
                                        <option value="0" disabled selected>-- Tipe Kamar--</option>
                                    </select>
                                </div>
                                <button type="button" class="btn btn-block btn-success btn-submit">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="rincian_paket" class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="display: block;">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Rincian Paket</h3>
                </div>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="row">
                            <table class="table no-border paket-place">
                                <tr>
                                    <td><label>Nama Paket</label></td>
                                    <td><span id="nama_paket"></span></td>
                                </tr>
                                <tr>
                                    <td><label>Jenis Paket</label></td>
                                    <td><span id="jenis_paket"></span></td>
                                </tr>
                                <tr>
                                    <td><label>Tanggal berangkat</label></td>
                                    <td><span id="tgl_brngkt"></span></td>
                                </tr>
                                <tr>
                                    <td><label>Lama Perjalanan</label></td>
                                    <td><span id="lama_perjalanan"></span></td>
                                </tr>
                                {{-- <tr>
                                    <td><label>Lainnya</label></td>
                                    <td>
                                        <div class="col-md-12">
                                            <div class="row">
                                                <button class="btn btn-primary" data-toggle="modal" data-target="#modal-default" onclick="openModalFasilitas()">Fasilitas</button>
                                                <button class="btn btn-primary" data-toggle="modal" data-target="#modal-default" onclick="openModalDokumen()">Dokumen</button>
                                                <button class="btn btn-primary" data-toggle="modal" data-target="#modal-default" onclick="openModalMaskapai()">Maskapai</button>
                                            </div>
                                        </div>
                                    </td>
                                </tr> --}}
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="{{ asset('/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{asset('AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{asset('AdminLTE/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script>
    $('#buat-order').addClass('active');
    $('#buat-order').closest('li.treeview').addClass('menu-open');
    $('#buat-order').closest('ul.treeview-menu').css('display', 'block');
    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    var paket_selected;
    function openModalFasilitas() {
        $.ajax({
            url: "{{ url('paket/daftar/fasilitas') }}",
            type: "GET",
            data :{
                "id_paket": paket_selected
            },
            success: function(response)
            {
                $('.modal-title').text("Fasilitas");
                var fasilitas_tersedia = response["include"].split(";");
                var li_fasilitas = "";
                var li_bukan_fasilitas = "";
                $.each(fasilitas_tersedia, function(key, value){
                    li_fasilitas += "<li>"+value+"</li>";
                });
                var fasilitas_tidak_tersedia = response["n_include"].split(";");
                $.each(fasilitas_tidak_tersedia, function(key, value){
                    li_bukan_fasilitas += "<li>"+value+"</li>";
                });
                $('.modal-body').html('<div class="col-md-12">\
                        <div class="row">\
                            <div class="col-md-6">\
                                <div class="form-group">\
                                    <label>Fasilitas tersedia</label>\
                                    <ul>'+li_fasilitas+'</ul>\
                                </div>\
                            </div>\
                            <div class="col-md-6">\
                                <div class="form-group">\
                                    <label>Fasilitas tidak tersedia</label>\
                                    <ul>'+li_bukan_fasilitas+'</ul>\
                                </div>\
                            </div>\
                        </div>\
                    </div>');
            }
        });
    }

    function openModalDokumen() {
        $.ajax({
            url: "{{ url('paket/daftar/dokumen') }}",
            type: "GET",
            data :{
                "id_paket": paket_selected
            },
            success: function(response)
            {
                $('.modal-title').text("Dokumen");
                var li_dokumen = "";
                $.each(response, function(key, value){
                    li_dokumen += "<li>"+value['dokumen']+" ("+value["catatan"]+")</li>";
                });
                $('.modal-body').html('<div class="col-md-12">\
                        <div class="row">\
                            <div class="form-group">\
                                <label>Dokumen dibutuhkan</label>\
                                <ul>'+li_dokumen+'</ul>\
                            </div>\
                        </div>\
                    </div>')
            }
        });
    }

    function openModalMaskapai() {
        $.ajax({
            url: "{{ url('paket/daftar/maskapai') }}",
            type: "GET",
            data :{
                "id_paket": paket_selected
            },
            success: function(response)
            {
                var place_maskapai = "";
                $('.modal-title').text("Maskapai");
                $.each(response, function(key, value) {
                    place_maskapai += '<div class="col-md-6 text-center" style="border: 2px solid;border-radius: 5px;">\
                                <img class="img-fluid" src="{{ asset("icon/maskapai/")}}/'+value['icon']+'" width="200px" height="200px">\
                                <table class="table">\
                                    <tr>\
                                        <td><label>Maskapai</label></td>\
                                        <td>'+value['nama']+'</td>\
                                    </tr>\
                                    <tr>\
                                        <td><label>Dari</label></td>\
                                        <td>'+value['dari']+'</td>\
                                    </tr>\
                                    <tr>\
                                        <td><label>Menuju</label></td>\
                                        <td>'+value['ke']+'</td>\
                                    </tr>\
                                </table>\
                            </div>'
                });
                $('.modal-body').html('<div class="col-md-12" style="margin-bottom: 10px">\
                        <div class="row">\
                            '+place_maskapai+'\
                        </div>\
                    </div>');
            }
        });
    }

    $('select[name="jenis_paket"]').on('change', function() {
        $.ajax({
            url: "{{ url('system/get/paket') }}",
            type: "GET",
            data :{
                "jenis_paket": $(this).children("option:selected").val()
            },
            success: function(response)
            {
                $('select[name="paket"]').html("");
                $('select[name="paket"]').append('<option value="0" disabled selected>--Pilih Paket--</option>');
                if (response.length === 0)
                {
                    $('select[name="paket"]').append("<option value='0' disabled selected>Tidak ada paket ditemukan</option>");
                } else {
                    $.each(response, function(key, value){
                        $('select[name="paket"]').append("<option value='"+value["id_paket"]+"'>"+value["nama_paket"]+"</option>");
                    });
                }
            }
        })
    });
    $('select[name="paket"]').on('change', function() {
        $('input[name="id_paket"]').val($(this).val());
        paket_selected = $(this).val();
        $.ajax({
            url: "{{ url('system/get/paket/detail') }}",
            type: "GET",
            data :{
                "id_paket": $(this).val()
            },
            success: function(response)
            {
                $('input[name="id_travel"]').val(response["id_travel"]);
                $('#nama_paket').text(response["nama_paket"]);
                $('#jenis_paket').text(response["kategori_paket"]);
                $('#tgl_brngkt').text(response["tgl_keberangkatan"]);
                $('#lama_perjalanan').text(response["lama_perjalanan"]+" Hari");
            }
        });
        $.ajax({
            url: "{{ url('paket/daftar/kamar') }}",
            type: "GET",
            data :{
                "id_paket": $(this).val()
            },
            success: function(response)
            {
                var option_tipe;
                $.each(response, function(key, value) {
                    option_tipe += "<option value='"+value['id_harga_paket']+"'>"+value['jenis']+" | Rp. "+numberWithCommas(value['harga'])+"</option>"
                });
                $('select[name="tipe-kamar"]').html(option_tipe);
            }
        });
    });
    
    $('input[name="email"]').on('blur', function(){
        var email_elem = $(this)
        email_elem.parent().removeClass('has-error');
        email_elem.parent().find('span').remove();
        $.ajax({
            url: '{{ url("system/check/alamat_email") }}',
            type: 'GET',
            data: {
                params: email_elem.val()
            },
            success: function(response){
                if (response != 0 || response != "0")
                {
                    email_elem.parent().addClass('has-error');
                    $('<span class="help-block">Email sudah terdaftar, silahkan gunakan email yang lain</span>').insertAfter(email_elem);
                    $('.btn-submit').attr('type', 'button');
                } else {
                    email_elem.parent().removeClass('has-error');
                    email_elem.parent().find('span').remove();
                    $('.btn-submit').attr('type', 'submit');
                }
            }
        })
    });
    $('input[name="phone"]').on('blur', function(){
        var email_elem = $(this)
        email_elem.parent().removeClass('has-error');
        email_elem.parent().find('span').remove();
        $.ajax({
            url: '{{ url("system/check/no_hp") }}',
            type: 'GET',
            data: {
                params: email_elem.val()
            },
            success: function(response){
                if (response != 0 || response != "0")
                {
                    email_elem.parent().addClass('has-error');
                    $('<span class="help-block">No Handphone sudah terdaftar, silahkan gunakan No Handphone yang lain</span>').insertAfter(email_elem);
                    $('.btn-submit').attr('type', 'button');
                } else {
                    email_elem.parent().removeClass('has-error');
                    email_elem.parent().find('span').remove();
                    $('.btn-submit').attr('type', 'submit');
                }
            }
        })
    })
    $('.datepicker').datepicker({
      autoclose: true
    })
</script>
@endsection