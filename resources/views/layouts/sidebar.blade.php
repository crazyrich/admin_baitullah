<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            <li>
                <a href="#">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            @if (Auth::user()->kode_travel == "SUPERADMIN")
                <li class="treeview">
                    <a href="#"><i class="fa fa-bold"></i> Banner
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li id="banner"><a title="Daftar Paket" href="{{url('/banners')}}"><i class="fa fa-gear"></i> Pengaturan Banner</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-globe"></i>
                        <span>Admin Travel</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li id="create-admin"><a title="input admin travel" href="{{url('/travel/admin')}}"><i class="fa fa-plus"></i> Input Admin Travel</a></li>
                        <li id="daftar-admin"><a title="Daftar Admin Travel" href="{{url('/travel/daftar-admin')}}"><i class="fa fa-align-justify"></i> Daftar Admin Travel</a></li>
                    </ul>
                </li>
            @endif
            <li class="treeview">
                <a href="#"><i class="fa fa-bars"></i> Kategori
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                @if (Auth::user()->kode_travel == "SUPERADMIN")
                    <li id="create-kategori"><a title="Tambah Kategori" href="{{url('/kategori')}}"><i class="fa fa-plus"></i> Tambah Kategori</li>
                @endif
                    <li id="daftar-kategori"><a title="Daftar Kategori" href="{{ url('kategori/daftar')}}"><i class="fa fa-align-justify"></i> Daftar Kategori</a></li>
                </ul>
            </li>
            @if (Auth::user()->kode_travel == "SUPERADMIN")
                <li class="treeview">
                    <a href="#"><i class="fa fa-bus"></i> Travel
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a title="Tambah Travel" href="{{url('/travel')}}"><i class="fa fa-plus"></i> Tambah Travel</a></li>
                        <li><a title="Daftar Travel" href="{{ url('/travel/daftar') }}"><i class="fa fa-align-justify"></i> Daftar Travel</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-plane"></i> Maskapai
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li id="create-maskapai"><a title="Tambah Maskapai" href="{{url('maskapai/')}}"><i class="fa fa-plus"></i> Tambah Maskapai</a></li>
                        <li id="daftar-maskapai"><a title="Daftar Maskapai" href="{{ url('maskapai/daftar') }}"><i class="fa fa-align-justify"></i> Daftar Maskapai</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-hotel"></i> Hotel
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li id="create-hotel"><a title="Tambah Hotel" href="{{url('hotel/buat')}}"><i class="fa fa-plus"></i> Tambah Hotel</a></li>
                        <li id="daftar-hotel"><a title="Daftar Hotel" href="{{ route('home-hotel') }}"><i class="fa fa-align-justify"></i> Daftar Hotel</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-user"></i> Pengguna</a>
                </li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-money"></i> Metode Pembayaran</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li id="tambah-pembayaran"><a href="{{ url('/metode-pembayaran/tambah') }}" title="Tambah Metode Pembayaran"><i class="fa fa-circle-o"></i> Tambah Metode Pembayaran</a></li>
                        <li id="daftar-pembayaran"><a href="{{ url('/metode-pembayaran/daftar') }}" title="Daftar Metode Pembayaran"><i class="fa fa-align-justify"></i> Daftar Metode pembayaran</a></li>
                    </ul>
                </li>
            @endif
            <li class="treeview">
                <a href="#"><i class="fa fa-folder-open"></i> Dokumen
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li id="dokumen"><a title="Tambah Travel" href="{{url('/dokumen')}}"><i class="fa fa-plus"></i> Tambah Dokumen</a></li>
                    {{-- <li><a title="Daftar Travel" href="{{ url('/dokumen/daftar') }}"><i class="fa fa-align-justify"></i> Daftar Dokumen</a></li> --}}
                </ul>
            </li>
            @if (Auth::user()->kode_travel != "SUPERADMIN")
                <li class="treeview">
                    <a href="#"><i class="fa fa-archive"></i> Paket
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li id="create-paket"><a title="Tambah Paket" href="{{url('/paket')}}"><i class="fa fa-plus"></i> Buat Paket</a></li>
                        <li id="daftar-paket"><a title="Daftar Paket" href="{{ url('/paket/daftar') }}"><i class="fa fa-align-justify"></i> Daftar Paket</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-cart-arrow-down"></i> Order</span>
                        <span class="pull-right-container">
                            <span class="label label-danger pull-right" id="order-count">0</span>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li id="buat-order"><a title="Pesanan belum dikonfirmasi" href="{{url('/pesanan/tambah')}}"><i class="fa fa-plus"></i> Buat Pesanan</a></li>
                        <li id="daftar-konfir"><a title="Pesanan belum dikonfirmasi" href="{{url('/pesanan/belum-di-proses')}}"><i class="fa fa-circle-o"></i> Menunggu Konfirmasi</a></li>
                        <li id="daftar-order"><a title="Daftar Transaksi" href="{{ url('/pesanan/all') }}"><i class="fa fa-align-justify"></i> Daftar Transaksi</a></li>
                    </ul>
                </li>
                <li id="daftar-jemaah">
                    <a href="{{ url('umrah/') }}"><i class="fa fa-paper-plane"></i> Siap Berangkat</span>
                    </a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-file"></i> Laporan</a>
                </li>
            @endif
        </ul>
    </section>
</aside>