@extends('layouts.master')
@section('content')
<section class="content-header">
    <h1>Kategori<small> Setting</small></h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Kategori</li>
    </ol>
</section>
<section class="content">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah Kategori</h3>
            </div>
            <div class="box-body">
                <form action="{{ url('travel/update') }}" method="POST" class="dropzone dropzone-custom needsclick add-professors" id="travel-upload" enctype="multipart/form-data">
                    <div class="row">
                        @csrf
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Kode Travel</label>
                                <input name="kode_travel" type="text" class="form-control" value="{{ $travel->kode_travel }}" readonly>
                            </div>
                            <div class="form-group">
                                <label>Nama Travel</label>
                                <input name="nama_travel" type="text" class="form-control" placeholder="Nama Travel" value="{{ $travel->nama_travel }}" required>
                            </div>
                            <div class="form-group">
                                <label>No Tlp</label>
                                <input name="no_telepon" type="number" class="form-control" placeholder="No Telp." value="{{ $travel->no_telepon }}" required>
                            </div>
                            <div class="form-group">
                                <label>No Hp/Whatsapp</label>
                                <input name="no_hp" type="number" class="form-control" placeholder="No Hp." value="{{ $travel->no_hp }}">
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input name="email" type="email" class="form-control" placeholder="Email." value="{{ $travel->email }}" required>
                            </div>
                            <div class="form-group res-mg-t-15">
                                <label>Alamat</label>
                                <textarea class="form-control" name="lokasi" placeholder="Address" required>{{ $travel->lokasi }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Icon</label>
                                <div class="form-group">
                                    <img src="{{ $travel->icon }}" class="img-fluid" id="icon-travel">
                                </div>
                                <input id="imgInp" name="icon-travel" type="file" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button> &nbsp;
                                <button id="clear" class="btn btn-primary waves-effect waves-light">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</section>                 
<script src="{{ asset('/js/jquery-3.3.1.min.js') }}"></script>
<script>
    function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function(e) {
        $('#icon-travel').attr('src', e.target.result);
        }
        
        reader.readAsDataURL(input.files[0]);
    }
    }

    $("#imgInp").change(function() {
    readURL(this);
    });
    $('#clear').on('click',function(){
        $(':input:not("[readonly]")','#travel-upload').not(':button, :submit, :reset, :hidden').val('')
    });
</script>
@endsection