@extends('layouts.master')
@section('css-here')
    <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('AdminLTE/dist/css/AdminLTE.min.css') }}">
@endsection
@section('content')
<section class="content-header">
    <h1>Travel<small> Setting</small></h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Travel</li>
    </ol>
</section>
<section class="content">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah Kategori</h3>
            </div>
            <div class="box-body">
                <form action="{{ url('travel/add') }}" method="POST" class="dropzone dropzone-custom needsclick add-professors" id="travel-upload" enctype="multipart/form-data">
                    <div class="row">
                        @csrf
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Kode Travel</label>
                                <input name="kode_travel" type="text" class="form-control" value="{{ $kode_travel }}" readonly>
                            </div>
                            <div class="form-group">
                                <label>Nama Travel</label>
                                <input name="nama_travel" type="text" class="form-control" placeholder="Nama Travel" required>
                            </div>
                            <div class="form-group">
                                <label>No Tlp</label>
                                <input name="no_telepon" type="number" class="form-control" placeholder="No Telepon." required>
                            </div>
                            <div class="form-group">
                                <label>No Hp/Whatsapp</label>
                                <input name="no_hp" type="number" class="form-control" placeholder="No Handphone." required>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input name="email" type="email" class="form-control" placeholder="Email Address" required>
                            </div>
                            <div class="form-group res-mg-t-15">
                                <label>Alamat</label>
                                <textarea name="lokasi" placeholder="Address" class="form-control" required></textarea>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6" style="margin-right: 10px;">
                                            <div class="row">
                                                <label>Propinsi</label>
                                                <select class="form-control select2 propinsi" style="width: 100% !important">
                                                    <option value="0" disabled selected>Pilih Propinsi</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                            <div class="row">
                                                <label>Kota</label>
                                                <select name="kota" class="form-control select2 kota" style="width: 100% !important">
                                                    <option value="0" disabled selected>Pilih Kota</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Icon</label>
                                <input name="icon-travel" type="file" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button> &nbsp;
                                <button type="button" id="clear" class="btn btn-danger waves-effect waves-light">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</section>
<script src="{{ asset('/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('AdminLTE/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script>
    $('#clear').on('click',function(){
        $(':input:not("[readonly]")','#travel-upload').not(':button, :submit, :reset, :hidden').val('')
    });
    $(document).ready(function() {
        $.ajax({
            url: "{{ url('get/propinsi/all') }}",
            type: "GET",
            success: function(response) {
                $.each(response, function(key, value) {
                    $('.propinsi').append("<option value='"+value["id"]+"'>"+value["name"]+"</option>");
                });
            }
        })
    })
    $('.propinsi').on('change', function() {
        $.ajax({
            url: "{{ url('get/kota') }}"+"/"+$(this).children('option:selected').val(),
            type: "GET",
            success: function(response) {
                $.each(response, function(key, value) {
                    $('.kota').append("<option value='"+value["id"]+"'>"+value["name"]+"</option>");
                });
            }
        })
    })
    $('.select2').select2()
</script>
@endsection