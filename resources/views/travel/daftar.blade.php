@extends('layouts.master')
@section('css-here')
    <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection
@section('content')
<section class="content-header">
    <h1>Kategori<small> Setting</small></h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Kategori</li>
    </ol>
</section>
<section class="content">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Daftar Travel</h3>
                </div>
                <div class="box-body">
                    <table class="table table-bordered" id="travel-tabel">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>ID</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Telepon</th>
                                <th>Alamat</th>
                                <th>Icon</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>

                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="{{ asset('/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{asset('AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script>
    $('#travel-tabel').DataTable({
            "processing": true,
            "serverSide": true,
            "ordering": true,
            "ajax": "/travel/daftar/json",
            "autoWidth" : false,
            "fixedColumns": true,
            "scrollX": "300px",
            "columns": [
                { "data": "no", "orderable": false },
                { "data": "kode_travel" },
                { "data": "nama_travel" },
                { "data": "email" },
                { "data": "no_telepon" },
                { "data": "lokasi" },
                { "data": "icon", "render": function(data, type, row){
                    return "<img src='{{ asset('icon') }}/"+data+"' class='img-responsive' width='50px'>"
                }},
                { "data": "id_travel", render: function(data, type, row){
                    var button_edit = "<form action='edit' method='POST' style='margin: 0; padding: 0;margin-block-end: 0;display: inline !important'><input type='hidden' name='_token' value='{{ csrf_token() }}'><input type='hidden' name='id_admin' value='"+data+"'><button type='submit' class='btn btn-success' data-toggle='tooltip' title='Edit' style='margin-right: 5px'><i class='fa fa-edit'></i></button></form>"
                    var button_delete = "<form action='delete' method='POST' style='margin: 0; padding: 0;margin-block-end: 0;display: inline !important'><input type='hidden' name='_token' value='{{ csrf_token() }}'><input type='hidden' name='id_admin' value='"+data+"'><button type='submit' class='btn btn-danger' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash'></i></button></form>"
                    return button_edit+button_delete
                } }
            ]
        })
</script>
@endsection