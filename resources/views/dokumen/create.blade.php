@extends('layouts.master')
@section('css-here')
    <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection
@section('content')
<section class="content-header">
    <h1>Dokumen<small> Setting</small></h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dokumen</li>
    </ol>
</section>
<section class="content">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah Dokumen</h3>
            </div>
            <div class="box-body">
                <form action="{{ url('dokumen/save') }}" method="POST" class="dropzone dropzone-custom needsclick add-professors" id="travel-upload" enctype="multipart/form-data">
                    <div class="row">
                        @csrf
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Kode Dokumen</label>
                                <input type="text" class="form-control" value="Auto Generate" readonly>
                            </div>
                            <div class="form-group">
                                <label>Jenis Dokumen</label>
                                <input name="dokumen" type="text" class="form-control" placeholder="ex. Passport" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button> &nbsp;
                                <button type="button" id="clear" class="btn btn-primary waves-effect waves-light">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Daftar Dokumen</h3>
            </div>
            <div class="box-body">
                <table class="table table-bordered" id="dokumen-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Dokumen</th>
                            <th>Author</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
</section>
<script src="{{ asset('/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{asset('AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script>
    $('#dokumen').addClass('active');
    $('#dokumen').closest('li.treeview').addClass('menu-open');
    $('#dokumen').closest('ul.treeview-menu').css('display', 'block');
    $('#dokumen-table').DataTable({
            "processing": true,
            "serverSide": true,
            "ordering": true,
            "ajax": "/dokumen/daftar/json",
            "autoWidth" : false,
            "fixedColumns": true,
            "scrollX": "300px",
            "columns": [
                { "data": "no", "orderable": false },
                { "data": "dokumen" },
                { "data": "author" },
                { "data": "id", render: function(data, type, row){
                    var button_delete = "<form action='dokumen/delete' method='POST' style='margin: 0; padding: 0;margin-block-end: 0;display: inline !important'><input type='hidden' name='_token' value='{{ csrf_token() }}'><input type='hidden' name='id' value='"+data+"'><button type='submit' class='btn btn-danger' data-toggle='tooltip' data-placement='left' title='Hapus'><i class='fa fa-trash'></i></button></form>"
                    return button_delete
                } }
            ]
        })
</script>
<script>
    $('#clear').on('click',function(){
        $(':input:not("[readonly]")','#travel-upload').not(':button, :submit, :reset, :hidden').val('')
    });
</script>
@endsection