@extends('layouts.master')
@section('css-here')
    <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection
@section('content')
<section class="content-header">
    <h1>Dokumen<small> Jemaah</small></h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Dokumen</li>
        <li class="active">Jemaah</li>
    </ol>
</section>
<section class="content">
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                    <strong>{{ $message }}</strong>
            </div>
        @endif
        @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                    <strong>{{ $message }}</strong>
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                Gagal membuat kategori baru : {{ $message }}
            </div>
        @endif
        <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Dokumen Jemaah</h3>
                </div>
                <div class="box-body">
                    <div>
                        <div class="form-group">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td width="10%">Nama</td>
                                        <td width="5%">:</td>
                                        <td>{{ $jemaah->nama_lengkap }}</td>
                                    </tr>
                                    <tr>
                                        <td width="10%">No Handphone</td>
                                        <td width="5%">:</td>
                                        <td>{{ $jemaah->alamat_email }}</td>
                                    </tr>
                                    <tr>
                                        <td width="10%">Email</td>
                                        <td width="5%">:</td>
                                        <td>{{ $jemaah->no_hp }}</td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <table class="table table-bordered" id="travel-tabel">
                        <thead>
                            <tr>
                                <th width="20%">Dokumen</th>
                                <th width="10%">Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($dokumen as $dok)
                                <tr>
                                    <td>{{ $dok->dokumen }}</td>
                                    <td>
                                        @if ($dok->status == 0)
                                            <i class='fa fa-exclamation-circle' title='Belum Ada' style='margin-right: 2px; color:#f39c12 !important'></i><label>Belum Ada</label>
                                        @elseif($dok->status == 1)
                                            <i class='fa fa-check-circle' title='Ada' style='margin-right: 2px; color: green'></i><label>Ada</label>
                                        @else
                                            <i class='fa fa-remove-circle' title='Ditolak' style='margin-right: 2px; color: red'></i><label>Ditolak</label>
                                        @endif
                                    </td>
                                    <td>
                                        <div style="padding: 5px">
                                            <button class="btn btn-info"><i class="fa fa-eye"></i> Lihat Dokumen</button>
                                            <button class="btn btn-success" onclick="tambahDokumen({{$dok->id_dokumen}}, {{$jemaah->id}}, '{{$dok->dokumen}}')"><i class="fa fa-plus"></i> Tambah Dokumen</button>
                                            <button class="btn btn-banger" disabled><i class="fa fa-remove"></i>Tolak Dokumen</button>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal fade" id="modal-default">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Unggah Dokumen</h4>
                        </div>
                        <form action="{{ url('dokumen/jemaah/simpan') }}" method="POST">
                            <div class="modal-body">
                                <div class="form-group">
                                    @csrf
                                    <input type="hidden" name="id_user">
                                    <input type="hidden" name="id_dok">
                                    <label class="jenis-dok"></label>
                                    <input type="file" name="dok-file">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success pull-right" data-dismiss="modal"><i class="fa fa-save"></i> Simpan</button>
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><i class="fa fa-remove"></i> Close</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        </div>
    </div>
</section>
<script src="{{ asset('/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{asset('AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script>
    $('#daftar-jemaah').addClass('active');
    $('#daftar-jemaah').closest('li.treeview').addClass('menu-open');
    $('#daftar-jemaah').closest('ul.treeview-menu').css('display', 'block');
    function tambahDokumen(id_dok, id_user, dok)
    {
        console.log("dokumen", id_dok);
        console.log("user", id_user);
        $('input[name="id_user"').val(id_user);
        $('input[name="id_dok"').val(id_dok);
        $('.jenis-dok').text("Unggah "+dok);
        $('#modal-default').modal('show');
    }
</script>
@endsection