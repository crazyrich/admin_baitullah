@extends('layouts.master')
@section('content')
<section class="content-header">
    <h1>Maskapai<small> Setting</small></h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Maskapai</li>
    </ol>
</section>
<section class="content">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah Maskapai</h3>
            </div>
            <div class="box-body">
                <form action="{{ url('maskapai/update') }}" method="POST" class="dropzone dropzone-custom needsclick add-professors" id="travel-upload" enctype="multipart/form-data">
                    <div class="row">
                        @csrf
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Kode Maskapai</label>
                                <input name="kode_maskapai" type="text" class="form-control" value="{{ $maskapai->kode_maskapai }}" readonly>
                            </div>
                            <div class="form-group">
                                <label>Nama Maskapai</label>
                                <input name="nama" type="text" class="form-control" placeholder="Nama Maskapai" value="{{ $maskapai->nama }}">
                            </div>
                            <div class="form-group">
                                <label>No Telepon</label>
                                <input name="no_tlp" type="number" class="form-control" placeholder="No Telepon." value="{{ $maskapai->no_tlp }}">
                            </div>
                            <div class="form-group">
                                <label>Fax</label>
                                <input name="no_fax" type="number" class="form-control" placeholder="No Fax." value="{{ $maskapai->no_fax }}">
                            </div>
                            <div class="form-group">
                                <label>Website</label>
                                <input name="website" type="text" class="form-control" placeholder="website" value="{{ $maskapai->website }}">
                            </div>
                            <div class="form-group res-mg-t-15">
                                <label>Alamat</label>
                                <textarea class="form-control" name="alamat" placeholder="Alamat">{{ $maskapai->alamat }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Icon</label>
                                <div class="form-group">
                                    <img src="{{ asset('icon/'.$maskapai->icon) }}" class="img-fluid" id="icon-maskapai" width="100px">
                                </div>
                                <input name="icon-maskapai" type="file" class="form-control" id="imgInp">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button> &nbsp;
                                <button type="button" id="clear" class="btn btn-primary waves-effect waves-light">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</section>
<script src="{{ asset('/js/jquery-3.3.1.min.js') }}"></script>
<script>
    $('#create-maskapai').addClass('active');
    $('#create-maskapai').closest('li.treeview').addClass('menu-open');
    $('#create-maskapai').closest('ul.treeview-menu').css('display', 'block');
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
            $('#icon-maskapai').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgInp").change(function() {
    readURL(this);
    });
    $('#clear').on('click',function(){
        $(':input:not("[readonly]")','#travel-upload').not(':button, :submit, :reset, :hidden').val('')
    });
</script>
@endsection