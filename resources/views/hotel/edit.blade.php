@extends('layouts.master')
@section('content')
<section class="content-header">
    <h1>Hotel<small> Setting</small></h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Hotel</li>
    </ol>
</section>
<section class="content">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Edit Hotel</h3>
            </div>
            <div class="box-body">
                <form action="{{ url('hotel/update') }}" method="POST" class="dropzone dropzone-custom needsclick add-professors" id="travel-upload" enctype="multipart/form-data">
                    <div class="row">
                        @csrf
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Kode Hotel</label>
                                <input name="id" type="text" class="form-control" value="{{ $hotel->id }}" readonly>
                            </div>
                            <div class="form-group">
                                <label>Nama Hotel</label>
                                <input name="name" type="text" class="form-control" placeholder="Nama Hotel" value="{{ $hotel->name }}">
                            </div>
                            <div class="form-group">
                                <label>No Telepon</label>
                                <input name="no_tlp" type="number" class="form-control" placeholder="No Telepon." value="{{ $hotel->no_tlp }}">
                            </div>
                            <div class="form-group">
                                <label>Website</label>
                                <input name="website" type="text" class="form-control" placeholder="Webiste" value="{{ $hotel->website }}">
                            </div>
                            <div class="form-group res-mg-t-15">
                                <label>Alamat</label>
                                <textarea class="form-control" name="alamat" placeholder="Alamat">{{ $hotel->alamat }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Bintang</label>
                                <input name="bintang" type="number" class="form-control" placeholder="ex. 3" value="{{ $hotel->bintang }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button> &nbsp;
                                <button type="button" id="clear" class="btn btn-primary waves-effect waves-light">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</section>
<script src="{{ asset('/js/jquery-3.3.1.min.js') }}"></script>
<script>
    $('#create-hotel').addClass('active');
    $('#create-hotel').closest('li.treeview').addClass('menu-open');
    $('#create-hotel').closest('ul.treeview-menu').css('display', 'block');
    $('#clear').on('click',function(){
        $(':input:not("[readonly]")','#travel-upload').not(':button, :submit, :reset, :hidden').val('')
    });
</script>
@endsection