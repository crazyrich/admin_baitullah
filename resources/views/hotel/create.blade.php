@extends('layouts.master')
@section('content')
<section class="content-header">
    <h1>Hotel<small> Setting</small></h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Hotel</li>
    </ol>
</section>
<section class="content">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah Hotel</h3>
            </div>
            <div class="box-body">
                <form action="{{ url('hotel/add') }}" method="POST" class="dropzone dropzone-custom needsclick add-professors" id="travel-upload" enctype="multipart/form-data">
                    <div class="row">
                        @csrf
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Kode Hotel</label>
                                <input name="kode_maskapai" type="text" class="form-control" value="Auto Generate" readonly>
                            </div>
                            <div class="form-group">
                                <label>Nama Hotel</label>
                                <input name="name" type="text" class="form-control" placeholder="Nama Hotel" required>
                            </div>
                            <div class="form-group">
                                <label>No Telepon</label>
                                <input name="no_tlp" type="number" class="form-control" placeholder="No Telepon." required>
                            </div>
                            <div class="form-group">
                                <label>Website</label>
                                <input name="website" type="text" class="form-control" placeholder="Webiste ('-' if empty)" required>
                            </div>
                            <div class="form-group res-mg-t-15">
                                <label>Alamat</label>
                                <textarea class="form-control" name="alamat" placeholder="Alamat" required></textarea>
                            </div>
                            <div class="form-group res-mg-t-15">
                                <label>Lokasi</label>
                                <input name="lokasi" type="text" class="form-control" placeholder="ex. Mekkah" required>
                            </div>
                            <div class="form-group">
                                <label>Bintang</label>
                                <input name="bintang" type="number" class="form-control" placeholder="ex. 3" required>
                            </div>
                            <div class="form-group">
                                <label>Icon</label>
                                <input name="icon" type="file" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Gambar</label>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="box box-widget widget-user-2">
                                            <div class="widget-user-header">
                                                <img class="img-responsive" src="{{ asset('icon/camera-icon-circle-21.png') }}" alt="User Avatar">
                                            </div>
                                            <div class="box-footer no-padding">
                                                <ul class="nav nav-stacked">
                                                    <li>
                                                        <input type="file" class="img-new" name="gambar-hotel[]" required>
                                                    </li>
                                                    <li>
                                                        <button type="button" class="btn btn-danger btn-block buang-gambar"><i class="fa fa-remove"></i> Buang</button>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="box box-widget widget-user-2">
                                            <div class="widget-user-header">
                                                <img class="img-responsive" id="tambah-gambar" src="{{ asset('icon/plus-png-image-59147.png') }}" alt="User Avatar">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button> &nbsp;
                                <button type="button" id="clear" class="btn btn-danger waves-effect waves-light">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</section>
<script src="{{ asset('/js/jquery-3.3.1.min.js') }}"></script>
<script>
    $('#create-hotel').addClass('active');
    $('#create-hotel').closest('li.treeview').addClass('menu-open');
    $('#create-hotel').closest('ul.treeview-menu').css('display', 'block');
    $('#clear').on('click',function(){
        $(':input:not("[readonly]")','#travel-upload').not(':button, :submit, :reset, :hidden').val('')
    });
    
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $(input).parent().parent().parent().parent().find('.img-responsive').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }

    $('#tambah-gambar').on('click', function() {
        $('<div class="col-md-2">\
                        <div class="box box-widget widget-user-2">\
                            <div class="widget-user-header">\
                                <img class="img-responsive" src="{{ asset('icon/camera-icon-circle-21.png') }}" alt="User Avatar">\
                            </div>\
                            <div class="box-footer no-padding">\
                                <ul class="nav nav-stacked">\
                                    <li>\
                                        <input type="file" class="img-new" name="gambar-hotel[]" required>\
                                    </li>\
                                    <li>\
                                        <button type="button" class="btn btn-danger btn-block buang-gambar"><i class="fa fa-remove"></i> Buang</button>\
                                    </li>\
                                </ul>\
                            </div>\
                        </div>\
                    </div>').insertBefore($(this).parent().parent().parent())
    });
    $( document ).on('click', '.buang-gambar', function() {
        if ($('.img-new').length <= 1 )
            alert('gambar tidak bisa kosong')
        else 
            $(this).parent().parent().parent().parent().parent().remove()
    });
    $( document ).on('change', '.img-new', function() {
        readURL(this)
    });
</script>
@endsection