@extends('layouts.master')
@section('content')
<section class="content-header">
    <h1>Kategori<small> Setting</small></h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Kategori</li>
    </ol>
</section>
<section class="content">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Daftar Admin Travel</h3>
            </div>
            <div class="box-body" style="background-color: #ecf0f5">
            <select name="travel" class="form-control" style="margin-bottom: 10px">
                <option value="all">Semua Travel</option>
                @foreach($travels as $travel)
                    <option value="{{$travel->kode_travel}}">{{$travel->nama_travel}}</option>
                @endforeach
            </select>
                <div class="row" id="place">
                <div class="col-md-4">
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-yellow">
              <div class="widget-user-image">
                <img class="img-circle" src="../dist/img/user7-128x128.jpg" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h3 class="widget-user-username">Nadia Carmichael</h3>
              <h5 class="widget-user-desc">Lead Developer</h5>
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                <li><a href="#">Projects <span class="pull-right badge bg-blue">31</span></a></li>
                <li><a href="#">Tasks <span class="pull-right badge bg-aqua">5</span></a></li>
                <li><a href="#">Completed Projects <span class="pull-right badge bg-green">12</span></a></li>
                <li><a href="#">Followers <span class="pull-right badge bg-red">842</span></a></li>
              </ul>
            </div>
          </div>
          <!-- /.widget-user -->
        </div>
                </div>
                <!-- /.col -->
            </div>
        </div>
    </div>
</section>
<script src="{{ asset('/js/jquery-3.3.1.min.js') }}"></script>
<script>
    $('#daftar-admin').addClass('active');
    $('#daftar-admin').closest('li.treeview').addClass('menu-open');
    $('#daftar-admin').closest('ul.treeview-menu').css('display', 'block');
    var travel_name = $('select[name="travel"] option:selected').val();
    $.ajax({
        url: '{{url("travel/get-list-admin")}}',
        type: 'GET',
        data: {
            'travel': 'all'
        },
        success: function(response) {
            $('#place').html("");
            $.each(response, function(key, value){
                $('#place').append('<div class="col-md-3">\
                                        <div class="box box-widget widget-user-2">\
                                            <div class="widget-user-header bg-yellow">\
                                                <div class="widget-user-image">\
                                                    <img class="img-circle" src="https://nellyrac.com/img/UserNoSign.png" alt="User Avatar">\
                                                </div>\
                                                <h3 class="widget-user-username">'+value["name"]+'</h3>\
                                                <h5 class="widget-user-desc">'+value["nama_travel"]+'</h5>\
                                            </div>\
                                            <div class="box-footer no-padding">\
                                                <ul class="nav nav-stacked">\
                                                    <li><a href="#">Icon Travel <img src="{{ asset("icon")}}/'+value["icon_travel"]+'" class="pull-right" width="25px"></a></li>\
                                                    <li><a href="#">Email <span class="pull-right">'+value["email"]+'</span></a></li>\
                                                    <li><a href="#">Phone Number <span class="pull-right badge bg-green">'+value["phone"]+'</span></a></li>\
                                                    <li><a href="#">Completed Projects <span class="pull-right badge bg-green">12</span></a></li>\
                                                    <li style="padding: 5px;">\
                                                        <form action="edit-admin" method="GET" style="margin-block-end: 0;">\
                                                            @csrf\
                                                            <input type="hidden" name="id_admin" value="'+value["id"]+'">\
                                                            <button type="submit" class="btn btn-default btn-block"><i class="fa fa-user"></i> Edit</button>\
                                                        </form>\
                                                    </li>\
                                                    <li style="padding: 5px;">\
                                                        <form action="delete-admin" method="POST" style="margin-block-end: 0;">\
                                                            @csrf\
                                                            <input type="hidden" name="id_admin" value="'+value["id"]+'">\
                                                            <button type="submit" class="btn btn-danger btn-block"><i class="fa fa-remove"></i> Delete</button>\
                                                        </form>\
                                                    </li>\
                                                </ul>\
                                            </div>\
                                        </div>\
                                    </div>');
            });
        }
    });
    $('select[name="travel"]').on('change', function(){
        var travel_kode = $(this).find("option:selected").val();
        $.ajax({
        url: '{{url("travel/get-list-admin")}}',
        type: 'GET',
        data: {
            'travel': travel_kode
        },
        success: function(response) {
            $('#place').html("");
            $.each(response, function(key, value){
                $('#place').append('<div class="col-md-3">\
                                        <div class="box box-widget widget-user-2">\
                                            <div class="widget-user-header bg-yellow">\
                                                <div class="widget-user-image">\
                                                    <img class="img-circle" src="https://nellyrac.com/img/UserNoSign.png" alt="User Avatar">\
                                                </div>\
                                                <h3 class="widget-user-username">'+value["name"]+'</h3>\
                                                <h5 class="widget-user-desc">'+value["nama_travel"]+'</h5>\
                                            </div>\
                                            <div class="box-footer no-padding">\
                                                <ul class="nav nav-stacked">\
                                                    <li><a href="#">Icon Travel <img src="'+value["icon_travel"]+'" class="pull-right" width="25px"></a></li>\
                                                    <li><a href="#">Email <span class="pull-right">'+value["email"]+'</span></a></li>\
                                                    <li><a href="#">Phone Number <span class="pull-right badge bg-green">'+value["phone"]+'</span></a></li>\
                                                    <li><a href="#">Completed Projects <span class="pull-right badge bg-green">12</span></a></li>\
                                                    <li style="padding: 5px;">\
                                                        <form action="edit-admin" method="GET" style="margin-block-end: 0;">\
                                                            @csrf\
                                                            <input type="hidden" name="id_admin" value="'+value["id"]+'">\
                                                            <button type="submit" class="btn btn-default btn-block"><i class="fa fa-user"></i> Edit</button>\
                                                        </form>\
                                                    </li>\
                                                    <li style="padding: 5px;">\
                                                        <form action="delete-admin" method="POST" style="margin-block-end: 0;">\
                                                            @csrf\
                                                            <input type="hidden" name="id_admin" value="'+value["id"]+'">\
                                                            <button type="submit" class="btn btn-danger btn-block"><i class="fa fa-remove"></i> Delete</button>\
                                                        </form>\
                                                    </li>\
                                                </ul>\
                                            </div>\
                                        </div>\
                                    </div>\
                                ');
                });
            }
        });
    });
    $('#clear').on('click',function(){
        $(':input:not("[readonly]")','#travel-upload').not(':button, :submit, :reset, :hidden').val('')
    });
</script>
@endsection