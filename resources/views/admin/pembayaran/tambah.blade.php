@extends('layouts.master')
@section('css-here')
    <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <style>
        .box.box-primary {
            border-top-color: #173404;
        }
        .box {
            border-top: 3px solid #173404;
        }
    </style>
@endsection
@section('content')
<section class="content-header">
    <h1>Metode Pembayaran<small> Setting</small></h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Metode Pembayaran</li>
    </ol>
</section>
<section class="content">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>	
            Gagal membuat metode baru : {{ $message }}
        </div>
    @endif
    <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">tambah metode pembayaran baru</h3>
            </div>
            <div class="box-body">
                <form action="{{url('metode-pembayaran/save')}}" method="POST">
                    @csrf
                    <div id="dropzone1" class="pro-ad">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" class="form-control" placeholder="ex. Virtual Account Bank Mandiri" name="nama">
                                </div>
                                <div class="form-group">
                                    <label>Tutorial</label>
                                    <textarea name="tutorial" id="editor1" cols="30" rows="10"></textarea>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-block btn-success" type="submit"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</section>
<script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
<script>
    var options = {
        filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
        filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token='+'{{ csrf_token() }}',
        filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
        filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='+'{{ csrf_token() }}'
    };
    var konten = document.getElementById("konten");
    CKEDITOR.replace('editor1', options);
</script>
<script src="{{ asset('/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('AdminLTE/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script>
    $('#tambah-pembayaran').addClass('active');
    $('#tambah-pembayaran').closest('li.treeview').addClass('menu-open');
    $('#tambah-pembayaran').closest('ul.treeview-menu').css('display', 'block');
    $('#t-fasilitas').on('click', function() {
        $('#inp-include').append('<input name="termasuk[]" type="text" class="form-control" placeholder="contoh: Tiket Pesawat PP">');
    });
    $('#tdk-fasilitas').on('click', function() {
        $('#inp-n-include').append('<input name="t_termasuk[]" type="text" class="form-control" placeholder="contoh: Tiket Pesawat PP">');
    });
    $( "#datepicker" ).datepicker(
    );
    var tempat_harga = $('#harga-place').html()
    var itenary_html = $('.itenary').html()
    var syarat_html = '<input name="syarat[]" type="text" class="form-control" placeholder="contoh: Umur diatas 17thn">'
    var ketentuan_html = '<input name="ketentuan[]" type="text" class="form-control" placeholder="contoh: Tiket yang sudah dibeli tidak dapat dikembalikan">'
    $('#btn-tipe').on('click', function(){
        $('#harga-place').append(tempat_harga);
    });
    $('#btn-itenary').on('click', function(){
        $('.itenary').append(itenary_html);
    });
</script>
@endsection