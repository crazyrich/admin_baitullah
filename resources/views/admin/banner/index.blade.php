@extends('layouts.master')
@section('css-here')
    <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('AdminLTE/dist/css/skins/_all-skins.min.css') }}">
@endsection
@section('content')
<section class="content-header">
    <h1>Kategori<small> Setting</small></h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Kategori</li>
    </ol>
</section>
<section class="content">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Pengaturan Banner</h3>
                </div>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <form action="{{ url('banners/save') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label>File Banner</label>
                                        <input type="file" class="form-control img-banner" name="banner">
                                    </div>
                                    <button type="submit" class="btn btn-block btn-success">Simpan</button>
                                </form>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Pratinjau Gambar</label>
                                    <img src="http://n7clothing.vn/images/no-thumbnail.png" class="img-responsive" width="400px">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" style="border-top: 1px solid">
                        <table class="table" id="banner-table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>ID</th>
                                    <th>Gambar</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- /.widget-user -->
            </div>
            <!-- /.modal -->
            <div class="modal fade" id="updateBanner">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title"><i class="fa fa-gear" style="color: red;"></i> Pengaturan banner</h4>
                        </div>
                        <form action="{{ url('banners/update') }}" method="POST">
                            @csrf
                            <input type="hidden" name="id-banner">
                            <input type="hidden" name="sts-banner">
                            <div class="modal-body">
                                <h3 id="label-banner"></h3>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-info pull-left" data-dismiss="modal" onclick="btnCancel()" style="font-size: 20px !important; font-weight: 700">Tidak</button>
                                <button type="submit" class="btn btn-danger" style="font-size: 20px !important; font-weight: 700">Ya</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        </div>
    </div>
</section>
<script src="{{ asset('/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{asset('AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script>
    $('#banner').addClass('active');
    $('#banner').closest('li.treeview').addClass('menu-open');
    $('#banner').closest('ul.treeview-menu').css('display', 'block');
    var check_status;
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $('.img-responsive').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    $( document ).on('change', '.img-banner', function() {
        readURL(this)
    });
    function confirmUpdate(id, status)
    {
        $('input[name="id-banner"]').val(id)
        if (status === 1) {
            $('input[name="sts-banner"]').val(1)
            $('#label-banner').text("Anda yakin ingin mengaktifkan banner ini ?")
        } else {
            $('input[name="sts-banner"]').val(0)
            $('#label-banner').text("Anda yakin ingin menonaktifkan banner ini ?")
        }
        $('#updateBanner').modal().show();
    }
    function btnCancel() {
        check_status.prop("checked", false);
    }
    $('#banner-table').DataTable({
        "processing": true,
        "serverSide": true,
        "ordering": true,
        "ajax": "/banners/daftar/json",
        "autoWidth" : false,
        "fixedColumns": true,
        "scrollX": "300px",
        "columns": [
            { "data": "no", "orderable": false },
            { "data": "id", "orderable": false },
            { "data": "path", "render": function(data, type, row){
                return "<img src='{{ asset('banner').'/' }}"+data+"' class='img-responsive' width='50px'>"
            }},
            { "data": "status", render: function(data, type, row){
                var checkbox_active;
                if (data === 1)
                    checkbox_active = '<label><input type="checkbox" class="flat-red active-status" checked>Tampilkan</label>';
                else
                    checkbox_active = '<label><input type="checkbox" class="flat-red active-status">Tampilkan</label>';
                return checkbox_active;
            }, "orderable": false },
            { "data": "id", render: function(data, type, row){
                var button_delete = "<form action='banners/delete' method='POST' style='margin: 0; padding: 0;margin-block-end: 0;display: inline !important'><input type='hidden' name='_token' value='{{ csrf_token() }}'><input type='hidden' name='id_admin' value='"+data+"'><button type='submit' class='btn btn-danger' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash'></i></button></form>"
                return button_delete
            } }
        ]
    });
    $(document).on('click', '.active-status', function(){
        check_status = $(this)
        var id = $(this).parent().parent().parent().find('td').text()[1];
        if ($(this).is(':checked')) {
            confirmUpdate(id, 1);
        } else {
            confirmUpdate(id, 0);
        }
    });
</script>
@endsection