@extends('layouts.master')
@section('content')
<section class="content-header">
    <h1>Admin<small> Setting</small></h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Admin</li>
    </ol>
</section>
<section class="content">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Edit Admin Travel</h3>
            </div>
            <div class="box-body">
                <form action="{{ url('travel/update-admin') }}" method="POST" class="dropzone dropzone-custom needsclick add-professors" id="travel-upload" enctype="multipart/form-data">
                    <div class="row">
                        @csrf
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h1><span class="blog-ht">Edit Admin Travel</span></h1>
                            <div class="form-group">
                                <input name="id_admin" type="hidden" class="form-control" value="{{$admins->id}}">
                                <input name="nama_travel" type="text" class="form-control" value="{{$travels->nama_travel}}" readonly>
                            </div>
                            <div class="form-group">
                                <input name="name" type="text" class="form-control" value="{{$admins->name}}">
                            </div>
                            <div class="form-group">
                                <input name="phone" type="number" class="form-control" value="{{$admins->phone}}">
                            </div>
                            <div class="form-group">
                                <input name="email" type="email" class="form-control" value="{{$admins->email}}">
                            </div>
                            <div class="form-group res-mg-t-15">
                                <input name="password" type="password" class="form-control" placeholder="Password">
                            </div>
                            <div class="form-group res-mg-t-15">
                                <input name="password_confirmation" type="password" class="form-control" placeholder="Konfirmasi Password">
                            </div>
                            <hr/>
                            <label>Konfirmasi password (superadmin)</label>
                            <div class="form-group res-mg-t-15">
                                <input name="super_password_confirmation" type="password" class="form-control" placeholder="Konfirmasi Password">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button> &nbsp;
                                <a href="{{ url('travel/daftar-admin') }}" id="clear" class="btn btn-primary waves-effect waves-light">Cancel</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</section>
<script src="{{ asset('/js/jquery-3.3.1.min.js') }}"></script>
<script>
    $('#create-admin').addClass('active');
    $('#create-admin').closest('li.treeview').addClass('menu-open');
    $('#create-admin').closest('ul.treeview-menu').css('display', 'block');
    $('#clear').on('click',function(){
        $(':input:not("[readonly]")','#travel-upload').not(':button, :submit, :reset, :hidden').val('')
    });
</script>
@endsection