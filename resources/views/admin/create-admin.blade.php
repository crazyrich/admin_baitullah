@extends('layouts.master')
@section('content')
<section class="content-header">
    <h1>Admin Travel<small> Setting</small></h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Admin Travel</li>
    </ol>
</section>
<section class="content">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah Admin Travel</h3>
            </div>
            <div class="box-body">
                <form action="{{ url('travel/create-admin') }}" method="POST" class="dropzone dropzone-custom needsclick add-professors" id="travel-upload" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="kode_travel">Kode Travel</label>
                        <select name="kode_travel" class="form-control">
                            <option selected disabled>-- Nama Travel --</option>
                            @foreach($travels as $travel)
                                <option value="{{$travel->kode_travel}}">{{$travel->nama_travel}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Nama</label>
                        <input name="name" type="text" class="form-control" placeholder="Nama" required>
                    </div>
                    <div class="form-group">
                        <label for="phone">No. Tlp</label>
                        <input name="phone" type="number" class="form-control" placeholder="No Telepon." required>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input name="email" type="email" class="form-control" placeholder="Alamat Email" required>
                    </div>
                    <div class="form-group res-mg-t-15">
                        <label for="password">Password</label>
                        <input name="password" type="password" class="form-control" placeholder="Password" required>
                    </div>
                    <div class="form-group res-mg-t-15">
                        <label for="password_confirmation">Password Confirmation</label>
                        <input name="password_confirmation" type="password" class="form-control" placeholder="Konfirmasi Password" required>
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button> &nbsp;
                        <button id="clear" class="btn btn-danger waves-effect waves-light">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</section>
<script src="{{ asset('/js/jquery-3.3.1.min.js') }}"></script>
<script>
    $('#create-admin').addClass('active');
    $('#create-admin').closest('li.treeview').addClass('menu-open');
    $('#create-admin').closest('ul.treeview-menu').css('display', 'block');
    $('#clear').on('click',function(){
        $(':input:not("[readonly]")','#travel-upload').not(':button, :submit, :reset, :hidden').val('')
    });
</script>
@endsection