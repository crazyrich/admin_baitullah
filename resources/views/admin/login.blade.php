<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Baitullah</title>

        <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('/css/baitullah.css') }}">
        <link href="https://fonts.googleapis.com/css?family=Lato&amp;display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
        <!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet"> -->

        <script src="{{ asset('/js/jquery-3.3.1.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="{{ asset ('/js/bootstrap.min.js') }}"></script>

        @yield('head')
		<base href="/">
        <style>
            body,html {
                height: 100%;
            }

            #profile-img {
                height:180px;
            }
            .h-80 {
                height: 80% !important;
            }
        </style>
    </head>
<body>
    <div class="container h-80">
        <div class="row align-items-center h-100">
            <div class="col-3 mx-auto">
                <div class="text-center">
                    <img src="{{asset('theme/img/logo/logo.jpeg')}}" class="rounded-circle" height="60px" style="margin-left: 2px;">
                    <p id="profile-name" class="profile-name-card"></p>
                    @if($errors->any())
                        <h4>{{$errors->first()}}</h4>
                    @endif
                    <form method="post" action="{{ url('/login') }}" class="form-signin">
                        {{csrf_field()}}
                        <input type="email" name="email" id="emailAgency" class="form-control form-group" placeholder="Email" required autofocus>
                        <input type="password" name="password" id="inputPassword" class="form-control form-group" placeholder="password" required>
                        <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit" style="background: #203c0e !important">Masuk</button>
                    </form><!-- /form -->
                </div>
            </div>
        </div>
    </div>
</body>
</html>