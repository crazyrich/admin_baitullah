@extends('layouts.master')
<style>
    .btn-file {
    position: relative;
    overflow: hidden;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }

    #img-upload{
        width: 100%;
    }
</style>
@section('content')
@section('content')
<section class="content-header">
    <h1>Paket<small> Setting</small></h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Paket</li>
    </ol>
</section>
<section class="content">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>	
            Gagal membuat kategori baru : {{ $message }}
        </div>
    @endif
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Paket #{{$nama->nama_paket}}</h3>
                </div>
                <div class="box-body">
                    <form action="{{url('paket/simpan-gambar')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id_paket" value="{{$paket}}">
                        <div class="row">
                            <div id="img-place">
                                <div class="gambar-class col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <label>Gambar</label>
                                    <div class="review-content-section">
                                        <input type="file" name="gambar_paket[]">
                                        <input name="display_order[]" type="text" class="form-control" placeholder="Urutan tampil">
                                    </div>
                                </div>
                            </div>
                            <button type="button" id="add-more" class="btn btn-success"><i class="fa fa-plus"></i></button>
                            <button type="button" id="min-more" class="btn btn-danger"><i class="fa fa-minus"></i></button>
                            <input class="btn btn-default" type="submit" value="Upload Image" name="submit">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="{{ asset('/js/jquery-3.3.1.min.js') }}"></script>
<script>
    $('#create-paket').addClass('active');
    $('#create-paket').closest('li.treeview').addClass('menu-open');
    $('#create-paket').closest('ul.treeview-menu').css('display', 'block');
    var img_div = $('#img-place').html();
    $('#add-more').on('click', function() {
        $('#img-place').append(img_div);
    });
    $('#min-more').on('click', function() {
        $('.gambar-class').last().remove()
    });
</script>
@endsection