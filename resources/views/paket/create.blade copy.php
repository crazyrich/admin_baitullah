@extends('layouts.master')
@section('css-here')
    <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('AdminLTE/dist/css/AdminLTE.min.css') }}">
    <style>
        .box.box-primary {
            border-top-color: #173404;
        }
        .box {
            border-top: 3px solid #173404;
        }
    </style>
@endsection
@section('content')
<section class="content-header">
    <h1>Paket<small> Setting</small></h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Paket</li>
    </ol>
</section>
<section class="content">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>	
            Gagal membuat kategori baru : {{ $message }}
        </div>
    @endif
    <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Paket</h3>
            </div>
            <div class="box-body">
                <form action="{{url('paket/add')}}" method="POST">
                    @csrf
                    <div id="dropzone1" class="pro-ad">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <h4><span class="blog-ht">Tambah Paket baru</span></h4>
                                <div class="form-group">
                                    <label>Travel</label>
                                    <input name="id_travel" type="text" class="form-control" value="{{$travel->nama_travel}}" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Nama Paket</label>
                                    <input name="nama_paket" type="text" class="form-control" placeholder="Umroh Reguler" required>
                                </div>
                                <div class="form-group">
                                    <label>Kategori Paket</label>
                                    <select name="kategori" class="form-control" required>
                                        <option value="#" selected disabled>-- Kategori --</option>
                                        @foreach($kategori as $k_paket)
                                            <option value="{{$k_paket->id_kategori_paket}}">{{$k_paket->kategori_paket}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Tanggal Berangkat</label>
                                    <input name="tgl_keberangkatan" type="text" class="form-control" placeholder="Tanggal berangkat" id="datepicker" required>
                                </div>
                                <div class="form-group res-mg-t-15">
                                    <label>Lama Perjalanan</label>
                                    <input name="lama_perjalanan" type="text" class="form-control" placeholder="9 Hari" required>
                                </div>
                                <div class="form-group res-mg-t-15">
                                    <label>Keberangkatan</label>
                                    <input name="berangkat_dari" class="form-control" placeholder="contoh: Jakarta" required>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <h4><span class="blog-ht">Perjalanan</span></h4>
                                <div class="form-group">
                                    <label>Perjalanan</label>
                                    <input name="pemberangkatan" type="text" class="form-control" value="Pergi" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Maskapai</label>
                                    <select name="maskapai" class="form-control" required>
                                        <option value="#" selected disabled>-- Maskapai --</option>
                                        @foreach($maskapai as $maskapais)
                                            <option value="{{$maskapais->id}}">{{$maskapais->nama}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Bandara Awal</label>
                                    <input name="dari" class="form-control" placeholder="contoh: Jakarta (CGK)" required>
                                </div>
                                <div class="form-group res-mg-t-15">
                                    <label>Bandara Tujuan</label>
                                    <input name="ke" type="text" class="form-control" placeholder="contoh: Jeddah (JED)" required>
                                </div>
                                <hr/>
                                <div class="form-group">
                                    <label>Perjalanan</label>
                                    <input name="pemberangkatan" type="text" class="form-control" value="Pulang" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Maskapai</label>
                                    <select name="maskapai-pulang" class="form-control" required>
                                        <option value="#" selected disabled>-- Maskapai --</option>
                                        @foreach($maskapai as $maskapais)
                                            <option value="{{$maskapais->id}}">{{$maskapais->nama}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Bandara Awal</label>
                                    <input name="pulang-dari" class="form-control" placeholder="contoh: Jeddah (JED)" required>
                                </div>
                                <div class="form-group res-mg-t-15">
                                    <label>Bandara Tujuan</label>
                                    <input name="pulang-ke" type="text" class="form-control" placeholder="contoh: Jakarta (CGK)" required>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h4><span class="blog-ht">Hotel</span></h4>
                            </div>
                            <div id="hotel">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px !important">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-bottom: 5px;">
                                        <div class="form-group">
                                            <label>Lokasi</label>
                                            <select name="lokasi[]" class="form-control" required>
                                                <option selected disabled>Pilih Lokasi Hotel</option>
                                                @foreach ($hotel as $lokasi)
                                                    <option value="{{ $lokasi->lokasi }}">{{ $lokasi->lokasi }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label>Hotel</label>
                                            <select name="hotel[]" class="form-control" required>
                                                
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <button type="button" id="btn-hotel" class="btn btn-success btn-block"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h4><span class="blog-ht">Tipe Kamar</span></h4>
                            </div>
                            <div id="harga-place">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px !important">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-bottom: 5px;">
                                        <div class="form-group">
                                            <label>Tipe</label>
                                            <input name="tipe_kamar[]" type="text" class="form-control" placeholder="contoh: Single" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label>Harga</label>
                                            <input name="harga_kamar[]" type="number" class="form-control" placeholder="contoh: 20000000" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <button type="button" id="btn-tipe" class="btn btn-success btn-block"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h4><span class="blog-ht">Fasilitas</span></h4>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-bottom: 5px;">
                                <div class="form-group" id="inp-include">
                                    <label>Termasuk</label>
                                    <input name="termasuk[]" type="text" class="form-control" placeholder="contoh: Tiket Pesawat PP" required>
                                </div>
                                <div id="sec-btn">
                                    <button type="button" id="t-fasilitas" class="btn btn-success"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-bottom: 5px;">
                                <div class="form-group" id="inp-n-include">
                                    <label>Tidak Termasuk</label>
                                    <input name="t_termasuk[]" type="text" class="form-control" placeholder="contoh: Tiket Pesawat PP" required>
                                </div>
                                <div id="sec-btn-tdk">
                                    <button type="button" id="tdk-fasilitas" class="btn btn-success"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                        </div>
                        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                        <hr/>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h4><span class="blog-ht">Itenary</span></h4>
                            </div>
                            <div class="itenary">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-bottom: 5px;">
                                    <div class="form-group">
                                        <label>Hari ke</label>
                                        <input name="hari[]" type="number" class="form-control" placeholder="hanya angke (contoh: 1)" required>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-bottom: 5px;">
                                    <div class="form-group">
                                        <label>Kegiatan</label>
                                        <input name="kegiatan[]" type="text" class="form-control" placeholder="ex. Berangkat dari jakarta" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <button type="button" id="btn-itenary" class="btn btn-success btn-block"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h4><span class="blog-ht">Dokumen dibutuhkan</span></h4>
                            </div>
                            <div class="dokumen">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div class="form-group">
                                        <label>Dokumen</label>
                                        <select name="dokumen[]" class="form-control select2" required>
                                            @foreach ($dokumen as $item)
                                                <option value="{{ $item->id }}">{{ $item->dokumen }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div class="form-group">
                                        <label>Catatan</label>
                                        <input name="dok_catatan[]" type="text" class="form-control" placeholder="ex. Visa dalam keadaan aktif" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <button type="button" id="btn-dokumen" class="btn btn-success btn-block"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h4><span class="blog-ht">Syarat & Ketentuan</span></h4>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 5px;">
                                <div class="form-group syarat">
                                    <textarea id="konten" class="form-control" name="sk" rows="10" cols="50" required></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px !important;">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Simpan & Tambah Gambar</button> &nbsp;
                                    <!-- <button type="submit" class="btn btn-primary waves-effect waves-light">Cancel</button> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</section>
<script src="{{ asset('/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('AdminLTE/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{asset('ckeditor/ckeditor.js')}}"></script>
<script src="{{ asset('AdminLTE/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script>
    $('#create-paket').addClass('active');
    $('#create-paket').closest('li.treeview').addClass('menu-open');
    $('#create-paket').closest('ul.treeview-menu').css('display', 'block');
    var dokumen_html = $('.dokumen').html();
    $('.select2').select2();
    var konten = document.getElementById("konten");
    CKEDITOR.replace(konten,{
        language:'en-gb'
    });
    CKEDITOR.config.allowedContent = true;
    $('#t-fasilitas').on('click', function() {
        $('#inp-include').append('<input name="termasuk[]" type="text" class="form-control" placeholder="contoh: Tiket Pesawat PP">');
    });
    $('#tdk-fasilitas').on('click', function() {
        $('#inp-n-include').append('<input name="t_termasuk[]" type="text" class="form-control" placeholder="contoh: Tiket Pesawat PP">');
    });
    $( "#datepicker" ).datepicker(
    );
    var tempat_harga = $('#harga-place').html()
    var itenary_html = $('.itenary').html()
    var hotel_html = $('#hotel').html()
    var syarat_html = '<input name="syarat[]" type="text" class="form-control" placeholder="contoh: Umur diatas 17thn" required>'
    var ketentuan_html = '<input name="ketentuan[]" type="text" class="form-control" placeholder="contoh: Tiket yang sudah dibeli tidak dapat dikembalikan" required>'
    $('#btn-tipe').on('click', function(){
        $('#harga-place').append(tempat_harga);
    });
    $('#btn-itenary').on('click', function(){
        $('.itenary').append(itenary_html);
    });
    $('#btn-dokumen').on('click', function(){
        $('.dokumen').append(dokumen_html);
    });
    $('#btn-hotel').on('click', function(){
        $('#hotel').append(hotel_html)
    });
    $( document ).on('change', 'select[name="lokasi[]"]', function() {
        var select_parent = $(this).parent().parent().parent()
        console.log($(this).children('option:selected').val())
        select_parent.find('select[name="hotel[]"]').children().remove()
        $.ajax({
            url: "{{ url('/hotel/daftar/lokasi') }}",
            type: "GET",
            data : {
                "lokasi": $(this).children('option:selected').val()
            },
            success: function(response) {
                console.log(response)
                $.each(response, function(key, value) {
                    select_parent.find('select[name="hotel[]"]').append("<option value='"+value["id"]+"'>"+value["name"]+"</option>")
                })
            }
        })
    });
</script>
@endsection