@extends('layouts.master')
@section('css-here')
    <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('AdminLTE/dist/css/AdminLTE.min.css') }}">
    <style>
        .box.box-primary {
            border-top-color: #173404;
        }
        .box {
            border-top: 3px solid #173404;
        }
        input.invalid {
            background-color: #ffdddd;
        }

        /* Hide all steps by default: */
        .tab {
            display: none;
        }

        /* Make circles that indicate the steps of the form: */
        .step {
            height: 15px;
            width: 15px;
            margin: 0 2px;
            background-color: #bbbbbb;
            border: none; 
            border-radius: 50%;
            display: inline-block;
            opacity: 0.5;
        }

        /* Mark the active step: */
        .step.active {
            opacity: 1;
        }

        /* Mark the steps that are finished and valid: */
        .step.finish {
            background-color: #4CAF50;
        }
    </style>
@endsection
@section('content')
<section class="content-header">
    <h1>Paket<small> Setting</small></h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Paket</li>
    </ol>
</section>
<section class="content">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>	
            Gagal membuat kategori baru : {{ $message }}
        </div>
    @endif
    <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Paket</h3>
            </div>
            <div class="box-body">
                <form action="{{url('paket/add')}}" method="POST" id="paketForm" enctype="multipart/form-data">
                    @csrf
                    <div id="dropzone1" class="pro-ad">
                        <div class="row tab">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h4><span class="blog-ht">Tambah Paket baru</span></h4>
                                <div class="form-group">
                                    <label>Travel</label>
                                    <input name="id_travel" type="text" class="form-control" value="{{$travel->nama_travel}}" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Nama Paket</label>
                                    <input name="nama_paket" type="text" class="form-control" placeholder="Umroh Reguler" required>
                                </div>
                                <div class="form-group">
                                    <label>Kategori Paket</label>
                                    <select name="kategori" class="form-control" required>
                                        <option value="#" selected disabled>-- Kategori Paket --</option>
                                        @foreach($kategori as $k_paket)
                                            <option value="{{$k_paket->id_kategori_paket}}">{{$k_paket->kategori_paket}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Tanggal Berangkat</label>
                                    <input name="tgl_keberangkatan" type="text" class="form-control" placeholder="Tanggal berangkat" id="datepicker" required>
                                </div>
                                <div class="form-group res-mg-t-15">
                                    <label>Lama Perjalanan</label>
                                    <div class="input-group">
                                        <input name="lama_perjalanan" type="number" class="form-control" placeholder="9" required>
                                        <div class="input-group-addon">
                                            <span>Hari</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group res-mg-t-15">
                                    <label>Keberangkatan</label>
                                    <select name="berangkat_dari" class="form-control select2" required style="width: 100% !important;">
                                        @foreach ($kota as $k)
                                            <option value="{{ $k->id }}">{{ $k->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row tab">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Perjalanan</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <span>Ke</span>
                                                </div>
                                                <input name="perjalanan[]" type="text" class="form-control" value="1" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Maskapai</label>
                                            <select name="maskapai[]" class="form-control" required>
                                                <option value="#" selected disabled>-- Maskapai --</option>
                                                @foreach($maskapai as $maskapais)
                                                    <option value="{{$maskapais->id}}">{{$maskapais->nama}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Bandara Awal</label>
                                            <input name="bandara-awal[]" class="form-control" placeholder="contoh: Jakarta (CGK)" required>
                                        </div>
                                        <div class="form-group res-mg-t-15">
                                            <label>Bandara Tujuan</label>
                                            <input name="bandara-tujuan[]" type="text" class="form-control" placeholder="contoh: Jeddah (JED)" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 maskapai-section">
                                        <div class="form-group">
                                            <label>Perjalanan</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <span>Ke</span>
                                                </div>
                                                <input name="perjalanan[]" type="text" class="form-control" value="2" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Maskapai</label>
                                            <select name="maskapai[]" class="form-control" required>
                                                <option value="#" selected disabled>-- Maskapai --</option>
                                                @foreach($maskapai as $maskapais)
                                                    <option value="{{$maskapais->id}}">{{$maskapais->nama}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Bandara Awal</label>
                                            <input name="bandara-awal[]" class="form-control" placeholder="contoh: Jeddah (JED)" required>
                                        </div>
                                        <div class="form-group res-mg-t-15">
                                            <label>Bandara Tujuan</label>
                                            <input name="bandara-tujuan[]" type="text" class="form-control" placeholder="contoh: Jakarta (CGK)" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <button type="button" class="btn btn-block btn-success" id="btn-maskapai"><i class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row tab">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h4><span class="blog-ht">Hotel</span></h4>
                            </div>
                            <div id="hotel">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px !important">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-bottom: 5px;">
                                        <div class="form-group">
                                            <label>Lokasi</label>
                                            <select name="lokasi[]" class="form-control" required>
                                                <option value="#" selected disabled>Pilih Lokasi Hotel</option>
                                                @foreach ($hotel as $lokasi)
                                                    <option value="{{ $lokasi->lokasi }}">{{ $lokasi->lokasi }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label>Hotel</label>
                                            <select name="hotel[]" class="form-control" required>
                                                
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <button type="button" id="btn-hotel" class="btn btn-success btn-block"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                        <div class="row tab">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h4><span class="blog-ht">Tipe Kamar</span></h4>
                            </div>
                            <div id="harga-place">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px !important">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-bottom: 5px;">
                                        <div class="form-group">
                                            <label>Tipe</label>
                                            <input name="tipe_kamar[]" type="text" class="form-control" placeholder="contoh: Single" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label>Harga</label>
                                            <input name="harga_kamar[]" type="number" class="form-control" placeholder="contoh: 20000000" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <button type="button" id="btn-tipe" class="btn btn-success btn-block"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                        <div class="row tab">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h4><span class="blog-ht">Fasilitas</span></h4>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-bottom: 5px;">
                                <div class="form-group" id="inp-include">
                                    <label>Termasuk</label>
                                    <input name="termasuk[]" type="text" class="form-control" placeholder="contoh: Tiket Pesawat PP" required>
                                </div>
                                <div id="sec-btn">
                                    <button type="button" id="t-fasilitas" class="btn btn-success"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-bottom: 5px;">
                                <div class="form-group" id="inp-n-include">
                                    <label>Tidak Termasuk</label>
                                    <input name="t_termasuk[]" type="text" class="form-control" placeholder="contoh: Tiket Pesawat PP" required>
                                </div>
                                <div id="sec-btn-tdk">
                                    <button type="button" id="tdk-fasilitas" class="btn btn-success"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                        </div>
                        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                        <div class="row tab">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h4><span class="blog-ht">Itenary</span></h4>
                            </div>
                            <div class="itenary">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-bottom: 5px;">
                                    <div class="form-group">
                                        <label>Hari ke</label>
                                        <input name="hari[]" type="number" class="form-control" placeholder="hanya angke (contoh: 1)" required>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-bottom: 5px;">
                                    <div class="form-group">
                                        <label>Kegiatan</label>
                                        <input name="kegiatan[]" type="text" class="form-control" placeholder="ex. Berangkat dari jakarta" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <button type="button" id="btn-itenary" class="btn btn-success btn-block"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                        <div class="row tab">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h4><span class="blog-ht">Dokumen dibutuhkan</span></h4>
                            </div>
                            <div class="dokumen">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div class="form-group">
                                        <label>Dokumen</label>
                                        <select name="dokumen[]" class="form-control" required>
                                            @foreach ($dokumen as $item)
                                                <option value="{{ $item->id }}">{{ $item->dokumen }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div class="form-group">
                                        <label>Catatan</label>
                                        <input name="dok_catatan[]" type="text" class="form-control" placeholder="ex. Visa dalam keadaan aktif" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <button type="button" id="btn-dokumen" class="btn btn-success btn-block"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                        <div class="row tab">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h4><span class="blog-ht">Syarat & Ketentuan</span></h4>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 5px;">
                                <div class="form-group syarat">
                                    <textarea id="konten" class="form-control" name="sk" rows="10" cols="50" required></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row tab">
                            <div id="img-place">
                                <div class="gambar-class col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <label>Gambar</label>
                                    <div class="review-content-section">
                                        <input type="file" name="gambar_paket[]">
                                    </div>
                                </div>
                            </div>
                            <button type="button" id="add-more" class="btn btn-success"><i class="fa fa-plus"></i></button>
                            <button type="button" id="min-more" class="btn btn-danger"><i class="fa fa-minus"></i></button>
                        </div>
                        <div class="row" style="margin-top: 10px !important;">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="text-right">
                                    <button type="button" class="btn btn-default waves-effect waves-light" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
                                    <button type="button" class="btn btn-primary waves-effect waves-light" id="nextBtn" onclick="nextPrev(1)">Next</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Circles which indicates the steps of the form: -->
                    <div style="text-align:center;margin-top:40px;">
                        <span class="step"></span>
                        <span class="step"></span>
                        <span class="step"></span>
                        <span class="step"></span>
                        <span class="step"></span>
                        <span class="step"></span>
                        <span class="step"></span>
                        <span class="step"></span>
                        <span class="step"></span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</section>
<script src="{{ asset('/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('AdminLTE/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{asset('ckeditor/ckeditor.js')}}"></script>
<script src="{{ asset('AdminLTE/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script>
    $('#create-paket').addClass('active');
    $('#create-paket').closest('li.treeview').addClass('menu-open');
    $('#create-paket').closest('ul.treeview-menu').css('display', 'block');
    var maskapai_html = $('.maskapai-section').html()
    var currentTab = 0; // Current tab is set to be the first tab (0)
    showTab(currentTab); // Display the current tab

    function showTab(n) {
    // This function will display the specified tab of the form ...
    var x = document.getElementsByClassName("tab");
    x[n].style.display = "block";
    // ... and fix the Previous/Next buttons:
    if (n == 0) {
        document.getElementById("prevBtn").style.display = "none";
    } else {
        document.getElementById("prevBtn").style.display = "inline";
    }
    if (n == (x.length - 1)) {
        document.getElementById("nextBtn").innerHTML = "Buat Paket";
    } else {
        document.getElementById("nextBtn").innerHTML = "Next";
    }
    // ... and run a function that displays the correct step indicator:
    fixStepIndicator(n)
    }

    function nextPrev(n) {
    // This function will figure out which tab to display
    var x = document.getElementsByClassName("tab");
    // Exit the function if any field in the current tab is invalid:
    if (n == 1 && !validateForm()) return false;
    if (n == 1 && !validateSelect()) return false;
    // Hide the current tab:
    x[currentTab].style.display = "none";
    // Increase or decrease the current tab by 1:
    currentTab = currentTab + n;
    // if you have reached the end of the form... :
    if (currentTab >= x.length) {
        //...the form gets submitted:
        document.getElementById("paketForm").submit();
        return false;
    }
    // Otherwise, display the correct tab:
    showTab(currentTab);
    }

    function validateSelect() {
        var x, y, i, valid_select = true;
        x = document.getElementsByClassName("tab");
        y = x[currentTab].getElementsByTagName("select");
        // A loop that checks every input field in the current tab:
        for (i = 0; i < y.length; i++) {
                // If a field is empty...
                if (y[i].value == "#") {
                // add an "invalid" class to the field:
                    y[i].closest("div").className += " has-error";
                // and set the current valid status to false:
                valid_select = false;
            }
        }
        if (valid_select) {
            for (i = 0; i < y.length; i++) {
                y[i].closest("div").classList.remove("has-error");
            }
            document.getElementsByClassName("step")[currentTab].className += " finish";
        }
        return valid_select; // return the valid status
    }

    function validateForm() {
    // This function deals with validation of the form fields
    var x, y, i, valid = true;
    x = document.getElementsByClassName("tab");
    y = x[currentTab].getElementsByTagName("input");
    // A loop that checks every input field in the current tab:
    for (i = 0; i < y.length; i++) {
        // If a field is empty...
        if (y[i].value == "") {
        // add an "invalid" class to the field:
        y[i].closest("div").className += " has-error";
        // and set the current valid status to false:
        valid = false;
        }
    }
    // If the valid status is true, mark the step as finished and valid:
    if (valid) {
        for (i = 0; i < y.length; i++) {
            y[i].closest("div").classList.remove("has-error");
        }
        document.getElementsByClassName("step")[currentTab].className += " finish";
    }
    return valid; // return the valid status
    }

    function fixStepIndicator(n) {
    // This function removes the "active" class of all steps...
    var i, x = document.getElementsByClassName("step");
    for (i = 0; i < x.length; i++) {
        x[i].className = x[i].className.replace(" active", "");
    }
    //... and adds the "active" class to the current step:
    x[n].className += " active";
    }
    var dokumen_html = $('.dokumen').html();
    $('.select2').select2();
    var konten = document.getElementById("konten");
    CKEDITOR.replace(konten,{
        language:'en-gb'
    });
    CKEDITOR.config.allowedContent = true;
    $('#t-fasilitas').on('click', function() {
        $('#inp-include').append('<input name="termasuk[]" type="text" class="form-control" placeholder="contoh: Tiket Pesawat PP">');
    });
    $('#tdk-fasilitas').on('click', function() {
        $('#inp-n-include').append('<input name="t_termasuk[]" type="text" class="form-control" placeholder="contoh: Tiket Pesawat PP">');
    });
    $( "#datepicker" ).datepicker(
    );
    var tempat_harga = $('#harga-place').html()
    var itenary_html = $('.itenary').html()
    var hotel_html = $('#hotel').html()
    var syarat_html = '<input name="syarat[]" type="text" class="form-control" placeholder="contoh: Umur diatas 17thn" required>'
    var ketentuan_html = '<input name="ketentuan[]" type="text" class="form-control" placeholder="contoh: Tiket yang sudah dibeli tidak dapat dikembalikan" required>'
    var img_div = $('#img-place').html();
    $('#btn-tipe').on('click', function(){
        $('#harga-place').append(tempat_harga);
    });
    $('#btn-itenary').on('click', function(){
        $('.itenary').append(itenary_html);
    });
    $('#btn-dokumen').on('click', function(){
        $('.dokumen').append(dokumen_html);
    });
    $('#btn-hotel').on('click', function(){
        $('#hotel').append(hotel_html)
    });
    $('#btn-maskapai').on('click', function(){
        var perjalanan = parseInt($('input[name="perjalanan[]"]:last').val())+1;
        $('<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">'+maskapai_html+'</div>').insertBefore($(this).parent());
        $('input[name="perjalanan[]"]:last').val(perjalanan)
    });
    $('#add-more').on('click', function() {
        $('#img-place').append(img_div);
    });
    $('#min-more').on('click', function() {
        $('.gambar-class').last().remove()
    });
    $( document ).on('change', 'select[name="lokasi[]"]', function() {
        var select_parent = $(this).parent().parent().parent()
        console.log($(this).children('option:selected').val())
        select_parent.find('select[name="hotel[]"]').children().remove()
        $.ajax({
            url: "{{ url('/hotel/daftar/lokasi') }}",
            type: "GET",
            data : {
                "lokasi": $(this).children('option:selected').val()
            },
            success: function(response) {
                console.log(response)
                $.each(response, function(key, value) {
                    select_parent.find('select[name="hotel[]"]').append("<option value='"+value["id"]+"'>"+value["name"]+"</option>")
                })
            }
        })
    });
</script>
@endsection