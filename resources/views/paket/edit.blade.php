@extends('layouts.master')
@section('css-here')
    <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('AdminLTE/dist/css/AdminLTE.min.css') }}">
    <style>
        .input-group .input-group-addon {
            background-color: #dd4b39;
            border-color: #d73925;
            color: #fff;
        }
    </style>
@endsection
@section('content')
<section class="content-header">
    <h1>Paket<small> Setting</small></h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Paket</li>
    </ol>
</section>
<section class="content">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>	
            @foreach ($errors->all() as $error)
                <span>Gagal membuat kategori baru : {{ $error }}</span>
            @endforeach
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger">{{ session('error') }}</div>
    @endif
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <!-- Custom Tabs (Pulled to the right) -->
        <div class="nav-tabs-custom">
        <ul class="nav nav-tabs pull-right">
            <li><a href="#dokumen-form" data-toggle="tab">Dokumen</a></li>
            <li><a href="#sk" data-toggle="tab">Syarat & Ketentuan</a></li>
            <li><a href="#itenary" data-toggle="tab">Itenary</a></li>
            <li><a href="#gambar" data-toggle="tab">Gambar</a></li>
            <li><a href="#fasilitas" data-toggle="tab">Fasilitas</a></li>
            <li><a href="#perjalanan" data-toggle="tab">Perjalanan</a></li>
            <li><a href="#hotel" data-toggle="tab">Hotel</a></li>
            <li class="active"><a href="#tab_3-2" data-toggle="tab">Detail</a></li>
            <li class="pull-left header"><i class="fa fa-th"></i> Edit Paket:  {{ $paket->nama_paket }}</li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane" id="hotel">
                <div class="row">
                    @foreach ($hotel as $h)
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px !important">
                            <input type="hidden" value="{{ $paket->id_paket}}" name="id_paket">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-bottom: 5px;">
                                <div class="form-group">
                                    <label>Lokasi</label>
                                    <select name="lokasi[]" class="form-control" disabled required>
                                        @foreach ($hotel_list as $lokasi)
                                            @if($h->lokasi == $lokasi->lokasi)
                                                <option value="{{ $lokasi->lokasi }}" selected>{{ $lokasi->lokasi }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>  
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-bottom: 5px;">
                                <div class="form-group">
                                    <label>Lokasi</label>
                                    <select name="hotel[]" class="form-control" disabled required>
                                        <option value="{{ $h->id }}" selected>{{ $h->name }}</option>
                                    </select>
                                </div>  
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 5px;">
                                <button class="btn btn-block btn-remove" onclick="hotelDelete({{$h->id}})"><i class="fa fa-trash"></i> Hapus</button>
                            </div>
                        </div>
                    @endforeach
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 5px;margin-top: 15px;">
                        <button type="button" class="btn btn-block btn-info" id="tambah-hotel"><i class="fa fa-plus"></i> Tambah</button>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="dokumen-form">
                <div class="row">
                    @foreach($dokumen as $dok)
                        <form action="{{ url('paket/dokumen/hapus') }}" method="POST">
                            @csrf
                            <input type="hidden" value="{{ $dok->id}}" name="id">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Dokumen</label>
                                    <input name="dokumen[]" type="text" class="form-control"  value="{{ $dok->dokumen }}" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Catatan</label>
                                    <input name="dok_catatan[]" type="text" class="form-control"  value="{{ $dok->dokumen_catatan }}" readonly>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-block btn-danger"><i class="fa fa-trash"></i> Hapus</button>
                                <hr />
                            </div>
                        </form>
                    @endforeach
                    <div class="col-md-12">
                        <button type="button" class="btn btn-block btn-success" id="tambah-dokumen"><i class="fa fa-plus"></i> Dokumen Baru</button>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="gambar">
                <form action="{{url('paket/update-gambar')}}" method="post" enctype="multipart/form-data">
                    <div class="row">
                        @csrf()
                        <input type="hidden" value="{{ $paket->id_paket}}" name="id_paket">
                        <div class="place-img">
                            @foreach($gambar as $g)
                                <div class="col-md-3">
                                    <div class="box box-widget widget-user-2">
                                        <div class="widget-user-header">
                                            <img class="img-responsive" src="{{ env('ADMIN_URL').$g->gambar}}" alt="User Avatar">
                                        </div>
                                        <div class="box-footer no-padding">
                                            <ul class="nav nav-stacked">
                                                <li><strong>Urutan tampil</strong> <input type="text" name="old-display-order[]" class="form-control" value="{{ $g->display_order }}"></li>
                                                <li>
                                                    <button type="button" class="btn btn-info btn-block ganti-gambar" style="background-color: #173404; border-color: #173c04"><i class="fa fa-photo"></i> Ganti</button>
                                                    <input type="file" class="img-new" name="old-gambar-paket[]" capture style="display:none" value="{{ env('ADMIN_URL').$g->gambar}}">
                                                    <input type="hidden" name="id-gambar-paket[]" capture style="display:none" value="{{ $g->id_gambar_paket }}">
                                                </li>
                                                <li>
                                                    <button type="button" class="btn btn-danger btn-block" onclick="deleteConfirm({{$g->id_gambar_paket}})"><i class="fa fa-remove"></i> Buang</button>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="col-md-3">
                            <div class="box box-widget widget-user-2">
                                <div class="widget-user-header">
                                    <!-- <input type="file" id="file1" name="image" accept="image/*" capture style="display:none"/> -->
                                    <img src="{{ asset('icon/plus-png-image-59147.png') }}" id="upfile1" style="cursor:pointer" class="img-responsive" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-block btn-success"><i class="fa fa-save"></i> Simpan</button>
                    <a href="{{ route('daftar-paket') }}" class="btn btn-block btn-danger"><i class="fa fa-remove"></i> Batal</a>
                </form>
            </div>
            <!-- /.tab-pane -->
            
            <div class="tab-pane" id="fasilitas">
                <form action="{{ url('paket/fasilitas/update') }}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <input type="hidden" value="{{ $paket->id_paket}}" name="id_paket">
                            <div class="form-group">
                                <label>Fasilitas termasuk</label>
                                @foreach(explode(";", $fasilitas->include) as $fasil)
                                    <div class="input-group" style="margin-bottom: 5px;">
                                        <input name="include[]" type="text" class="form-control"  value="{{ $fasil }}">    
                                        <div class="input-group-addon hapus">
                                            <i class="fa fa-remove"></i>
                                        </div>
                                    </div>
                                @endforeach
                                <button type="button" class="btn btn-success btn-block tambah"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <input type="hidden" value="{{ $paket->id_paket}}" name="id_paket">
                            <div class="form-group">
                                <label>Tidak termasuk fasilitas</label>
                                @foreach(explode(";", $fasilitas->n_include) as $fasil)
                                    <div class="input-group" style="margin-bottom: 5px;">
                                        <input name="n_include[]" type="text" class="form-control"  value="{{ $fasil }}">    
                                        <div class="input-group-addon hapus">
                                            <i class="fa fa-remove"></i>
                                        </div>
                                    </div>
                                @endforeach
                                <button type="button" class="btn btn-success btn-block tambah-n"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-block btn-success"><i class="fa fa-save"></i> Simpan</button>
                    <a href="{{ route('daftar-paket') }}" class="btn btn-block btn-danger"><i class="fa fa-remove"></i> Batal</a>
                </form>
            </div>

            <div class="tab-pane" id="sk">
                <form action="{{ url('paket/sk/update') }}" method="POST">
                    @csrf
                    <input type="hidden" value="{{ $paket->id_paket}}" name="id_paket">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 5px;">
                            <div class="form-group syarat">
                                <textarea id="konten" class="form-control" name="sk" rows="10" cols="50">{{ $sk->sk }}</textarea>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-block btn-success"><i class="fa fa-save"></i> Simpan</button>
                    <a href="{{ route('daftar-paket') }}" class="btn btn-block btn-danger"><i class="fa fa-remove"></i> Batal</a>
                </form>
            </div>

            <div class="tab-pane" id="itenary">
                <form action="{{ url('paket/itenary/update') }}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <ul class="timeline">
                                @foreach($itenary as $iten)
                                    <!-- timeline time label -->
                                    <li class="time-label">
                                        <span class="bg-red">
                                            Hari ke {{ $iten->hari }}
                                        </span>
                                    </li>
                                    <!-- /.timeline-label -->
                                    <!-- timeline item -->
                                    <li>
                                        <i class="fa fa-telegram bg-blue"></i>
                                        <div class="timeline-item">
                                            <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>
                                            <h3 class="timeline-header"><a href="#">{{ $iten->kegiatan }}</a></h3>
                                        </div>
                                    </li>
                                    <!-- END timeline item -->
                                @endforeach
                            </ul>
                        </div>
                        <div class="col-md-6">
                            @foreach($itenary as $iten)
                                <div class="form-group">
                                    <input type="hidden" value="{{ $paket->id_paket}}" name="id_paket">
                                    <div class="input-group" style="margin-bottom: 5px;">
                                        <div class="input-group-addon" style="background-color: #f4f4f4 !important;color: #444 !important;border-color: #ddd !important">
                                            Hari ke
                                        </div>
                                        <input name="hari[]" type="text" class="form-control" value="{{ $iten->hari }}">
                                    </div>
                                    <label>Kegiatan</label>
                                    <input type="text" name="kegiatan[]" class="form-control" value="{{ $iten->kegiatan }}">
                                    <button type="button" class="btn btn-danger btn-block" onclick="hapusIntenary({{ $iten->id }})" style="margin-top: 5px;"><i class="fa fa-remove"></i></button>
                                </div>
                                <hr/>
                            @endforeach
                            <div class="form-group">
                                <button type="button" class="btn btn-success btn-block tambah-itenary"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                    </div>
                    <button type="submit" id="simpan-itenary" class="btn btn-block btn-success" disabled><i class="fa fa-save"></i> Simpan</button>
                    <a href="{{ route('daftar-paket') }}" class="btn btn-block btn-danger"><i class="fa fa-remove"></i> Batal</a>
                </form>
            </div>
            
            <div class="tab-pane active" id="tab_3-2">
                <form action="{{url('paket/detail/update')}}" method="POST">
                    @csrf
                    <input type="hidden" value="{{ $paket->id_paket}}" name="id_paket">
                    <div class="form-group">
                        <label>Travel</label>
                        <input name="nama_travel" type="text" class="form-control"  value="{{ $travel->nama_travel }}" readonly>
                        <input name="id_travel" type="hidden" value="{{ $paket->id_travel }}">
                    </div>
                    <div class="form-group">
                        <label>Nama Paket</label>
                        <input name="nama_paket" type="text" class="form-control" value="{{ $paket->nama_paket }}">
                    </div>
                    <div class="form-group">
                        <label>Kategori Paket</label>
                        <select name="id_kategori" class="form-control">
                            @foreach($kategori as $k_paket)
                                @if($k_paket->id_kategori_paket == $paket->id_kategori)
                                    <option value="{{$k_paket->id_kategori_paket}}" selected>{{$k_paket->kategori_paket}}</option>
                                @else
                                    <option value="{{$k_paket->id_kategori_paket}}">{{$k_paket->kategori_paket}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Tanggal Berangkat</label>
                        <input name="tgl_keberangkatan" type="text" class="form-control" value="{{ $paket->tgl_keberangkatan }}" id="datepicker">
                    </div>
                    <div class="form-group res-mg-t-15">
                        <label>Lama Perjalanan</label>
                        <input name="lama_perjalanan" type="text" class="form-control" value="{{ $paket->lama_perjalanan }}">
                    </div>
                    <div class="form-group res-mg-t-15">
                        <label>Keberangkatan</label>
                        <select name="berangkat_dari" class="form-control select2" required style="width: 100% !important;">
                                <option value="{{ $paket->berangkat_dari }}">{{ $paket->berangkat }}</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-block btn-success"><i class="fa fa-save"></i> Simpan</button>
                    <a href="{{ route('daftar-paket') }}" class="btn btn-block btn-danger"><i class="fa fa-remove"></i> Batal</a>
                </form>
            </div>
            <!-- /.tab-pane -->

            <div class="tab-pane" id="perjalanan">
                <h4><span class="blog-ht">Perjalanan</span></h4>
                    @foreach ($rute as $r)
                        <form action="{{url('paket/rute/update')}}" method="POST">
                            @csrf
                            <input type="hidden" name="id" value="{{ $r->id }}">
                            <input type="hidden" name="nama_paket" value="{{ $paket->nama_paket }}">
                            <div class="form-group">
                                <label>Maskapai</label>
                                <select name="maskapai" class="form-control">
                                    @foreach($maskapai as $mas => $value)
                                        @if($value->id == $r->maskapai)
                                            <option value="{{ $value->id }}" selected>{{ $value->nama }}</option>
                                        @else 
                                            <option value="{{ $value->id }}">{{ $value->nama }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Perjalanan</label>
                                <div class="input-group">
                                    <div class="input-group-addon" style="background-color: #fff !important; border-color: #d2d6de !important; color: #000 !important">
                                        <span>Ke</span>
                                    </div>
                                    <input name="perjalanan" type="text" class="form-control" value="{{$r->pemberangkatan}}" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Bandara Awal</label>
                                <input name="dari" class="form-control" value="{{ $r->dari }}">
                            </div>
                            <div class="form-group res-mg-t-15">
                                <label>Bandara Tujuan</label>
                                <input name="ke" type="text" class="form-control" value="{{ $r->ke }}">
                            </div>
                            <button type="submit" class="btn btn-block btn-success"><i class="fa fa-save"></i> Simpan</button> 
                            <hr/>
                        </form>
                    @endforeach
                    <a href="{{ route('daftar-paket') }}" class="btn btn-block btn-danger"><i class="fa fa-remove"></i> Batal</a>
            </div>
        </div>
        <!-- /.tab-content -->
        <div class="modal fade" id="modal-default">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="fa fa-remove" style="color: red;"></i> Hapus Gambar</h4>
                    </div>
                    <form action="{{ url('paket/gambar/hapus') }}" method="POST">
                        @csrf
                        <input type="hidden" name="hapus-id-gambar">
                        <input type="hidden" name="id_paket" value="{{ $paket->id_paket }}">
                        <div class="modal-body">
                            <h3>Anda yakin ingin menghapus gambar tersebut ?</h3>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-info pull-left" data-dismiss="modal" style="font-size: 20px !important; font-weight: 700">Tidak</button>
                            <button type="submit" class="btn btn-danger" style="font-size: 20px !important; font-weight: 700">Ya</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <div class="modal fade" id="modal-hotel-delete">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="fa fa-remove" style="color: red;"></i> Hapus Hotel</h4>
                    </div>
                    <form action="{{ url('paket/hotel/hapus') }}" method="POST">
                        @csrf
                        <input type="hidden" name="hapus-id-hotel">
                        <input type="hidden" name="id_paket" value="{{ $paket->id_paket }}">
                        <div class="modal-body">
                            <h3>Anda yakin ingin menghapus Hotel tersebut ?</h3>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-info pull-left" data-dismiss="modal" style="font-size: 20px !important; font-weight: 700">Tidak</button>
                            <button type="submit" class="btn btn-danger" style="font-size: 20px !important; font-weight: 700">Ya</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <div class="modal fade" id="modal-itenary-delete">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="fa fa-remove" style="color: red;"></i> Hapus Itenary</h4>
                    </div>
                    <form action="{{ url('paket/itenary/hapus') }}" method="POST">
                        @csrf
                        <input type="hidden" name="hapus-id-itenary">
                        <input type="hidden" name="id_paket" value="{{ $paket->id_paket }}">
                        <div class="modal-body">
                            <h3>Anda yakin ingin menghapus Itenary Ini ?</h3>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-info pull-left" data-dismiss="modal" style="font-size: 20px !important; font-weight: 700">Tidak</button>
                            <button type="submit" class="btn btn-danger" style="font-size: 20px !important; font-weight: 700">Ya</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        </div>
        <!-- nav-tabs-custom -->
    </div>
<!-- /.col -->
</div>
</section>
<script src="{{ asset('/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('AdminLTE/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('AdminLTE/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script>
    $('#create-paket').addClass('active');
    $('#create-paket').closest('li.treeview').addClass('menu-open');
    $('#create-paket').closest('ul.treeview-menu').css('display', 'block');
    var konten = document.getElementById("konten");
    CKEDITOR.replace(konten,{
        language:'en-gb'
    });
    $('.select2').select2()
    function readURLOld(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $(input).parent().parent().parent().parent().find('.img-responsive').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    function deleteConfirm(id)
    {
        $('input[name="hapus-id-gambar"]').val(id)
        $('#modal-default').modal().show()
    }
    
    function hotelDelete(id)
    {
        $('input[name="hapus-id-hotel"]').val(id)
        $('#modal-hotel-delete').modal().show()
    }

    function hapusIntenary(id)
    {
        $('input[name="hapus-id-itenary"]').val(id)
        $('#modal-itenary-delete').modal().show()
    }

    $("#upfile1").click(function () {
        $('.place-img').append('<div class="col-md-3">\
                                    <div class="box box-widget widget-user-2">\
                                        <div class="widget-user-header bg-yellow">\
                                            <img class="img-responsive" alt="User Avatar">\
                                        </div>\
                                        <div class="box-footer no-padding">\
                                            <ul class="nav nav-stacked">\
                                                <li><strong>Urutan tampil</strong> <input type="text" name="display_order[]" class="form-control" value="{{ $g->display_order+1 }}"></li>\
                                                <li>\
                                                    <button type="button" class="btn btn-info btn-block ganti-gambar" style="background-color: #173404; border-color: #173c04"><i class="fa fa-photo"></i> Ganti</button>\
                                                    <input type="file" class="img-new" name="gambar-paket[]" capture style="display:none">\
                                                </li>\
                                                <li>\
                                                    <button type="button" class="btn btn-danger btn-block hapus-gambar"><i class="fa fa-remove"></i> Buang</button>\
                                                </li>\
                                            </ul>\
                                        </div>\
                                    </div>\
                                </div>')
    });
    $("#file1").change(function() {
        readURL(this);
    });
    $( "#datepicker" ).datepicker({
        format:'yyyy-mm-d'
    });
    
    $('.tambah').on('click', function(){
        $('<div class="input-group" style="margin-bottom: 5px;">\
                <input name="include[]" type="text" class="form-control" placeholder="Add On">\
                <div class="input-group-addon hapus" title="Hapus">\
                    <i class="fa fa-remove"></i>\
                </div>\
            </div>').insertBefore($(this));
    })
    $('.tambah-n').on('click', function(){
        $('<div class="input-group" style="margin-bottom: 5px;">\
                <input name="n_include[]" type="text" class="form-control" placeholder="Add On">\
                <div class="input-group-addon hapus" title="Hapus">\
                    <i class="fa fa-remove"></i>\
                </div>\
            </div>').insertBefore($(this));
    })

    $('.tambah-itenary').on('click', function(){
        $('<div class="form-group">\
                <div class="input-group" style="margin-bottom: 5px;">\
                    <div class="input-group-addon" style="background-color: #f4f4f4 !important;color: #444 !important;border-color: #ddd !important">\
                        Hari ke\
                    </div>\
                    <input name="hari-new[]" type="text" class="form-control" required>\
                </div>\
                <label>Kegiatan</label>\
                <input type="text" name="kegiatan-new[]" class="form-control" required>\
                <button type="button" class="btn btn-danger btn-block hapus-itenary" style="margin-top: 5px;"><i class="fa fa-remove"></i></button>\
            </div>\
            <hr/>').insertBefore($(this));
        $('#simpan-itenary').attr('disabled', false)
    });
    
    $('#tambah-dokumen').on('click', function() {
        var html_dokumen = '<form action="{{ url("paket/dokumen/create") }}" method="POST">\
                            @csrf\
                            <input type="hidden" value="{{ $paket->id_paket}}" name="id_paket">\
                            <div class="col-md-6">\
                                <div class="form-group">\
                                    <label>Dokumen</label>\
                                    <select class="form-control" name="dokumen[]">\
                                    </select>\
                                </div>\
                            </div>\
                            <div class="col-md-6">\
                                <div class="form-group">\
                                    <label>Catatan</label>\
                                    <input name="dok_catatan[]" type="text" class="form-control">\
                                </div>\
                            </div>\
                            <div class="col-md-12">\
                                <button type="submit" class="btn btn-block btn-info"><i class="fa fa-save"></i> Simpan</button>\
                                <hr />\
                            </div>\
                        </form>'
        $(html_dokumen).insertBefore($(this).parent())
        $.ajax({
            url: "{{ url('dokumen/all') }}",
            type: "GET",
            success: function(response){
                $('select[name="dokumen[]"]').children().remove()
                $.each(response, function(key, value) {
                    $('select[name="dokumen[]"]').append("<option value='"+value["id"]+"'>"+value["dokumen"]+"</option>")
                });
            }
        })
    })
    
    $( document ).on('click', '.hapus', function(){
        $(this).parent().remove();
    });

    $( document ).on('click', '.hapus-gambar', function(){
        $(this).parent().parent().parent().parent().parent().remove();
    });
    
    $( document ).on('click', '.ganti-gambar', function(){
        $(this).parent().find('.img-new').trigger('click');
    });
    $( document ).on('change', '.img-new', function(){
        readURLOld(this);
    });
    $( document ).on('click', '.hapus-itenary', function(){
        $(this).parent().remove()
    });
    $('#tambah-hotel').on('click', function() {
        var hotel_html = '<form action="{{ url("paket/tambah/hotel") }}" method="POST">\
                            @csrf\
                            <input type="hidden" value="{{ $paket->id_paket}}" name="id_paket">\
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px !important">\
                                <input type="hidden" value="{{ $paket->id_paket}}" name="id_paket">\
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-bottom: 5px;">\
                                    <div class="form-group">\
                                        <label>Lokasi</label>\
                                        <select name="lokasi[]" class="form-control" required>\
                                            @foreach ($hotel_list as $lokasi)\
                                                    <option value="{{ $lokasi->lokasi }}">{{ $lokasi->lokasi }}</option>\
                                            @endforeach\
                                        </select>\
                                    </div>\
                                </div>\
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-bottom: 5px;">\
                                    <div class="form-group">\
                                        <label>Lokasi</label>\
                                        <select name="hotel[]" class="form-control" required>\
                                            <option value="{{ $h->id }}" selected>{{ $h->name }}</option>\
                                        </select>\
                                    </div>\
                                </div>\
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 5px;">\
                                    <button type="button" class="btn btn-block btn-danger rollback-hotel"><i class="fa fa-remove"></i> Hapus</button>\
                                    <button type="submit" class="btn btn-block btn-success"><i class="fa fa-save"></i> Simpan</button>\
                                </div>\
                            </div>\
                        </form>'
        $(hotel_html).insertBefore($(this).parent())
    });
    $( document ).on('change', 'select[name="lokasi[]"]', function() {
        var select_parent = $(this).parent().parent().parent()
        select_parent.find('select[name="hotel[]"]').children().remove()
        $.ajax({
            url: "{{ url('/hotel/daftar/lokasi') }}",
            type: "GET",
            data : {
                "lokasi": $(this).children('option:selected').val()
            },
            success: function(response) {
                $.each(response, function(key, value) {
                    select_parent.find('select[name="hotel[]"]').append("<option value='"+value["id"]+"'>"+value["name"]+"</option>")
                })
            }
        })
    });
    $( document ).on('click','.rollback-hotel', function() {
        $(this).parent().parent().parent().remove()
    })
    $.ajax({
        url: "{{ url('get/kota/all') }}",
        type: "GET",
        success: function(response) {
            var select_active = $('select[name="berangkat_dari"]').children("option:selected").val();
            $.each(response, function(key, value) {
                if (value["id"] !== select_active) {
                    $('select[name="berangkat_dari"]').append("<option value='"+value["id"]+"'>"+value["name"]+"</option>");
                }
            });
        }
    })
</script>
@endsection