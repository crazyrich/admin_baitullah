@extends('layouts.master')
@section('css-here')
    <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection
@section('content')
<section class="content-header">
    <h1>Kategori<small> Setting</small></h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Kategori</li>
    </ol>
</section>
<section class="content">
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                    <strong>{{ $message }}</strong>
            </div>
        @endif
        @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                    <strong>{{ $message }}</strong>
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                Gagal membuat kategori baru : {{ $message }}
            </div>
        @endif
        <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Daftar Travel</h3>
                </div>
                <div class="box-body">
                    <table class="table table-bordered" id="travel-tabel">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kategori</th>
                                <th>Nama</th>
                                <th>Tgl Berangkat</th>
                                <th>Lama Perjalanan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <!-- /.modal -->
            <div class="modal fade" id="paket-delete">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title"><i class="fa fa-remove" style="color: red;"></i> Hapus Paket</h4>
                        </div>
                        <form action='delete' method='POST'>
                            @csrf
                            <input type="hidden" name="id_admin">
                            <div class="modal-body">
                                <h3>Anda yakin ingin menghapus Paket Ini ?</h3>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-info pull-left" data-dismiss="modal" style="font-size: 20px !important; font-weight: 700">Tidak</button>
                                <button type="submit" class="btn btn-danger" style="font-size: 20px !important; font-weight: 700">Ya</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
        </div>
    </div>
</section>
<script src="{{ asset('/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{asset('AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script>
    $('#daftar-paket').addClass('active');
    $('#daftar-paket').closest('li.treeview').addClass('menu-open');
    $('#daftar-paket').closest('ul.treeview-menu').css('display', 'block');
    function deletePaket(id)
    {
        $('#paket-delete').modal().show();
        $('input[name="id_admin"]').val(id)
    }
    $('#travel-tabel').DataTable({
            "processing": true,
            "serverSide": true,
            "ordering": true,
            "ajax": "/paket/daftar/json",
            "autoWidth" : false,
            "fixedColumns": true,
            "scrollX": "300px",
            "columns": [
                { "data": "no", "orderable": false },
                { "data": "kategori" },
                { "data": "nama_paket" },
                { "data": "tgl_keberangkatan" },
                { "data": "lama_perjalanan" },
                { "data": "id_paket", render: function(data, type, row){
                    var button_edit = "<form action='edit/"+data+"' method='GET' style='margin: 0; padding: 0;margin-block-end: 0;display: inline !important'><input type='hidden' name='_token' value='{{ csrf_token() }}'><button type='submit' class='btn btn-success' data-toggle='tooltip' data-placement='left' title='Edit' style='margin-right: 5px'><i class='fa fa-edit'></i></button></form>"
                    var button_delete = "<button type='button' class='btn btn-danger' onclick='deletePaket("+data+")' data-toggle='tooltip' data-placement='left' title='Hapus'><i class='fa fa-trash'></i></button>"
                    return button_edit+button_delete
                } }
            ]
        })
</script>
@endsection