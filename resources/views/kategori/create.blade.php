@extends('layouts.master')
@section('content')
<section class="content-header">
    <h1>Kategori<small> Setting</small></h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Kategori</li>
    </ol>
</section>
<section class="content">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>	
            Gagal membuat kategori baru : {{ $message }}
        </div>
    @endif
    <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah Kategori</h3>
            </div>
            <div class="box-body">
                <form action="{{url('kategori/add')}}" class="dropzone dropzone-custom needsclick add-professors" id="demo1-upload" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label>Kategori Baru</label>
                            <div class="form-group">
                                <input name="kategori_paket" type="text" class="form-control" placeholder="Nama Kategori" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="text-left">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button> &nbsp;
                                <button type="submit" class="btn btn-primary waves-effect waves-light">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</section>
<script src="{{ asset('/js/jquery-3.3.1.min.js') }}"></script>
<script>
    $('#create-kategori').addClass('active');
    $('#create-kategori').closest('li.treeview').addClass('menu-open');
    $('#create-kategori').closest('ul.treeview-menu').css('display', 'block');
</script>
@endsection