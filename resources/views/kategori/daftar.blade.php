@extends('layouts.master')
@section('css-here')
    <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection
@section('content')
<section class="content-header">
    <h1>Artikel<small> Setting</small></h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Artikel</li>
    </ol>
</section>
<section class="content">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>	
            Gagal membuat kategori baru : {{ $message }}
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Daftar Sumber Artikel</h3>
                </div>
                <div class="box-body">
                    <table class="table table-bordered" id="users-table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Kategori</th>
                                <th>Dibuat</th>
                                <th>Pembuat</th>
                                <th>Diperbarui</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($kategori as $kategoris)
                                <tr>
                                    <td>{{ $kategoris->id_kategori_paket }}</td>
                                    <td>{{ $kategoris->kategori_paket }}</td>
                                    <td>{{ $kategoris->created }}</td>
                                    <td>{{ $kategoris->author }}</td>
                                    <td>{{ $kategoris->updated }}</td>
                                    <td>
                                        <form action="{{ url('kategori/delete') }}" method="POST">
                                            @csrf
                                            <input type="hidden" value="{{ $kategoris->id_kategori_paket }}" name="id">
                                            <button type="submit" class="btn btn-danger"><i class="fa fa-remove"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="{{ asset('/js/jquery-3.3.1.min.js') }}"></script>
<script>
    $('#daftar-kategori').addClass('active');
    $('#daftar-kategori').closest('li.treeview').addClass('menu-open');
    $('#daftar-kategori').closest('ul.treeview-menu').css('display', 'block');
</script>
@endsection