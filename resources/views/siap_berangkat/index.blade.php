@extends('layouts.master')
@section('css-here')
    <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection
@section('content')
<section class="content-header">
    <h1>Umrah<small> Setting</small></h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Umrah</li>
    </ol>
</section>
<section class="content">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="modal fade" id="modal-default">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Daftar Jemaah</h4>
                    </div>
                    <div class="modal-body">
                        @include('siap_berangkat.daftar_jemaah')
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Jadwal Keberangkatan</h3>
                </div>
                <div class="box-body">
                    <table class="table table-bordered" id="pesanan-table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Paket</th>
                                <th>Tanggal Berangkat</th>
                                <th>Kota</th>
                                <th>Jumlah Jemaah</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="{{ asset('/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{asset('AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script>
    $('#daftar-jemaah').addClass('active');
    $('#daftar-jemaah').closest('li.treeview').addClass('menu-open');
    $('#daftar-jemaah').closest('ul.treeview-menu').css('display', 'block');
    var tabel_jemaah = $('#jemaah-table');
    function daftarJemaah(id)
    {
        tabel_jemaah.DataTable({
            "processing": true,
            "serverSide": true,
            "ordering": true,
            "ajax": {
                "url": "/dokumen/jemaah/json",
                "type": "GET",
                "data" : {
                    "id_paket": id
                }
            },
            "autoWidth" : false,            
            "columns": [
                { "data": "no", "orderable": false },
                { "data": "nama_lengkap", "orderable": false },
                { "data": "alamat_email", "orderable": false },
                { "data": "no_hp", "orderable": false },
                { "data": "dokumen", render: function(data, type, row){
                    var checkdok = "";
                    $.each(data, function(key, value) {
                        if (value["status"] === "0")
                            checkdok += "<div class='col-md-12'><div class='form-group'><i class='fa fa-exclamation-circle' title='Belum Ada' style='margin-right: 2px; color:#f39c12 !important'></i><label>"+value["dokumen"]+"</label></div></div>"
                        else if (value["status"] === "1")
                            checkdok += "<div class='col-md-12'><div class='form-group'><i class='fa fa-check-circle' title='Belum Ada' style='margin-right: 2px; color: green'></i><label>"+value["dokumen"]+"</label></div></div>"
                        else 
                            checkdok += "<div class='col-md-12'><div class='form-group'><i class='fa fa-remove' title='Belum Ada' style='margin-right: 2px; color: red'></i><label>"+value["dokumen"]+"</label></div></div>"
                    })
                    return checkdok
                }, "orderable": false },
                { "data": "id", render: function(data, type, row){
                    var button_edit = "<a href='{{ url('/dokumen/jemaah?id=') }}"+data+"' class='btn btn-success' data-toggle='tooltip' title='Edit data Jemaah' data-placement='left' style='margin-right: 5px'><i class='fa fa-edit'></i></button>"
                    var button_delete = "<form action='konfirmasi' method='POST' style='margin: 0; padding: 0;margin-block-end: 0;display: inline !important'><input type='hidden' name='_token' value='{{ csrf_token() }}'><input type='hidden' name='id_admin' value='"+data+"'><button type='submit' class='btn btn-warning' data-toggle='tooltip' data-placement='left' title='Sebarkan berita'><i class='fa fa-bullhorn'></i></button></form>"
                    return button_edit
                }, "orderable": false }
            ]
        });
        $('#modal-default').modal('show');
    }
    $('#pesanan-table').DataTable({
        "processing": true,
        "serverSide": true,
        "ordering": true,
        "ajax": "/umrah/ready/json",
        "autoWidth" : false,
        "fixedColumns": true,
        "scrollX": "300px",
        "columns": [
            { "data": "no", "orderable": false },
            { "data": "nama_paket", "orderable": false },
            { "data": "tgl_keberangkatan", "orderable": false },
            { "data": "kota", "orderable": false },
            { "data": "jemaah", "orderable": false },
            { "data": "id_paket", render: function(data, type, row){
                var button_edit = "<button type='button' class='btn btn-success' data-toggle='tooltip' title='Daftar Jemaah' data-placement='left' style='margin-right: 5px' onclick='daftarJemaah("+data+")'><i class='fa fa-users'></i></button>"
                var button_delete = "<form action='konfirmasi' method='POST' style='margin: 0; padding: 0;margin-block-end: 0;display: inline !important'><input type='hidden' name='_token' value='{{ csrf_token() }}'><input type='hidden' name='id_admin' value='"+data+"'><button type='submit' class='btn btn-warning' data-toggle='tooltip' data-placement='left' title='Sebarkan berita'><i class='fa fa-bullhorn'></i></button></form>"
                return button_edit+button_delete
            }, "orderable": false }
        ]
    })
</script>
@endsection